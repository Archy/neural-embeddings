#hack for importing from sibling dirs
import context

import math
import logging
import numpy as np
from typing import List, Dict
from sklearn.metrics import pairwise_distances

from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.graph.graph import Graph
from cat2vec.utils.common import create_node_idx_mapping, create_initial_weight


class NaiveEmbeddingCreation(EmbeddingModel):
    NAME = 'naive_full_adjacency_v1'

    def __init__(self, name:str, embedding_size: int, use_glove_init: bool) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.name = name
        self.embedding_size = embedding_size
        self.use_glove_init = use_glove_init

        self.W = None
        self.node2idx = {}
        self.idx2node = {}
        self.train_config = {}  # No training config

    def get_name(self) -> str:
        return self.name

    def fit_args(self, graph: Graph, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        return self.fit(graph)

    def fit(self, graph: Graph) -> List[float]:
        self.node2idx = node2idx = create_node_idx_mapping(graph)
        self.idx2node = {v: k for k, v in node2idx.items()}
        N = len(graph.nodes)

        if self.W is None:
            self.W = np.zeros((N, N))
            for node_idx, node in graph.nodes:
                node_i = node2idx[node_idx]
                for child in node.children:
                    child_i = node2idx[child.idx]
                    self.W[node_i, child_i] = 1

        return []

    def predict(self, node_idx: int, n: int, metric: str) -> List[int]:
        node_vec = self.W[self.node2idx[node_idx]]
        distances = pairwise_distances(node_vec.reshape(1, self.embedding_size), self.W, metric=metric) \
            .reshape(len(self.node2idx))
        idx = distances.argsort()[1:n]

        return list(map(lambda i: self.idx2node[i], idx))

    def get_training_params(self) -> Dict[str, object]:
        assert self.train_config is not None
        return self.train_config

    def get_node2idx(self) -> Dict[int, int]:
        return self.node2idx

    def get_embeddings(self) -> np.ndarray:
        return self.W

    def save(self, dst_path: str, name_prefix: str) -> List[str]:
        # nothing to save really
        pass

    def load(self, dst_path: str, name_prefix: str) -> None:
        raise Exception('Loading naive glove based embedding not supported')
