# Cat2Vec - neural embeddings for Wikipedia category graph
Application of DeepWalk, Glove, SDNE (more to come) for creating neural embeddings for Wikipedia categories graph. Implemented in Python and TensorFlow.

Done as a master thesis at Gdansk University of Technology.

## Directory structure 
```
.
├── cat2vec             # actual implementation
├── notebooks           # jupyter notebooks used for experimenting
├── scripts             # bunch of scripts used during development
├── test                # unit tests for cat2vec
├── wiki-graph          # subgraphs of wikipedia category graph used for developing cat2vec
├── requirements.txt    # cat2vec requirements.
...
```

## Prerequisites
All required dependencies are listed in ```requirements.txt```. 
After installing them you also need to install following corporas  from ```NLTK Data``` (see [NLTK Corpora](http://www.nltk.org/nltk_data/)).
You can to it by running ```scripts/init_nltk.py``` script. Required corporas:
- Punkt Tokenizer Models (punkt)
- Averaged Perceptron Tagger (averaged_perceptron_tagger)
- WordNet ('wordnet')

To use weights initialization based on categories names you also need to manually download [GloVe 6B](https://nlp.stanford.edu/projects/glove/) and adjust ```GLOVE_6B_BASE_PATH``` in 
```cat2vec\utils\common.py```. 

## Further improvements:
- [ ] use ```networkx ``` for graph representation
- [ ] improve performance of algorithms (eg. concurrency for deep walk)

## Others:
- Running tensorboard:
```
tensorboard --logdir=C:\Users\Jan\Desktop\neural-embeddings\tensorboard
```
- Visualizing graph in yWork Graph Editor:
   - create .graphml file using scripts/wiki_to_graphml
   - load created file to yWork 
   - in yWork map 'name' data to actual label text: ```edit > properties mapper``` 