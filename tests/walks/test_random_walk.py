import random
import unittest
import cat2vec.walks.random_walk as random_walk
from cat2vec.graph.graph import Graph


def create_test_graph() -> Graph:
    g = Graph()
    g.add_node(1, 'a')
    g.add_node(2, 'b')
    g.add_node(3, 'c')
    g.add_node(4, 'd')
    g.add_node(5, 'd')
    g.add_node(6, 'd')
    g.add_node(7, 'd')
    g.add_node(8, 'd')

    g.add_edges(1, [2, 3])
    g.add_edges(2, [4])
    g.add_edges(3, [5, 6, 7])
    g.add_edges(4, [8])
    g.add_edges(5, [8])
    g.add_edges(6, [8])
    g.add_edges(8, [1])

    return g


def create_path_graph() -> Graph:
    g = Graph()
    g.add_node(1, 'a')
    g.add_node(2, 'b')
    g.add_node(3, 'c')
    g.add_node(4, 'd')

    g.add_edges(1, [2])
    g.add_edges(2, [3])
    g.add_edges(3, [4])

    return g


class RandomWalkTest(unittest.TestCase):
    def test_simple_walk(self):
        graph = create_test_graph()

        walk = random_walk.random_walk(graph, 4, None)
        self.assertEqual(len(walk), 4)
        self.assertEqual(len(set(walk)), 4)

        walk = random_walk.random_walk(graph, 3, graph.nodes[2])
        self.assertEqual(walk, [2, 4, 8])

    def test_walk_with_restart(self):
        random.seed(42)  # make the test repeatable
        graph = create_test_graph()

        walk = random_walk.random_walk(graph, 10, graph.nodes[1], 0.2)
        print(walk)
        self.assertEqual(walk, [1, 2, 4, 8, 1, 2, 1, 2, 4, 8])

    def test_path_graph(self):
        path = create_path_graph()
        walk = random_walk.random_walk(path, 4, path.nodes[1])
        self.assertEqual(walk, [1, 2, 3, 4])

        # test that walk is shorten than required if there's nowhere to go
        walk = random_walk.random_walk(path, 6, path.nodes[1])
        self.assertEqual(walk, [1, 2, 3, 4])

    def test_create_walks_list(self):
        random.seed(42)  # make the test repeatable
        graph = create_test_graph()

        walks = random_walk.random_walks(graph, 2, 3, 0.2)
        walks.sort(key=lambda w: w[0])
        print(walks)

        expected = [[1, 3, 1], [1, 3, 7], [2, 4, 8], [2, 4, 8], [3, 5, 8], [3, 7], [4, 8, 1], [4, 8, 4], [5, 5, 8], [5, 5, 8], [6, 8, 1], [6, 8, 1], [8, 1, 3], [8, 1, 2]]
        self.assertListEqual(walks, expected)


if __name__ == '__main__':
    unittest.main()
