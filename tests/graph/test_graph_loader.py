# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import unittest
import os.path

import cat2vec.graph.loader as loader
from cat2vec.graph.graph import Graph


class GraphLoaderTest(unittest.TestCase):
    def test_graph_loading(self):
        resources_dir = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')), 'resources', 'graph')
        DICT_PATH = os.path.join(resources_dir, 'cat_dict.txt')
        CAT_PATH = os.path.join(resources_dir, 'categories.txt')

        self.assertTrue(os.path.isfile(DICT_PATH))
        self.assertTrue(os.path.isfile(CAT_PATH))
        graph = loader.load_graph(CAT_PATH, DICT_PATH)

        expected_graph = Graph()
        expected_graph.add_node(1, 'root')
        expected_graph.add_node(2, 'child_1')
        expected_graph.add_node(3, 'child_2')
        expected_graph.add_node(4, 'leaf_1')
        expected_graph.add_node(5, 'leaf_2')
        expected_graph.add_node(6, 'leaf_3')

        expected_graph.add_edges(1, [2, 3])
        expected_graph.add_edges(2, [4, 5])
        expected_graph.add_edges(3, [5, 6])
        expected_graph.add_edges(6, [2])

        self.assertEqual(graph, expected_graph)
        self.assertNotEqual(graph, Graph())


if __name__ == '__main__':
    unittest.main()
