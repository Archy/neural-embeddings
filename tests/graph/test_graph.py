# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import unittest
from cat2vec.graph.graph import Node
from cat2vec.graph.graph import Graph


class GraphTest(unittest.TestCase):
    def test_node_eq(self):
        n1 = Node(1, 'aaa')
        n2 = Node(1, 'aaa')
        n3 = Node(2, 'aaa')
        n4 = Node(1, 'b')

        self.assertTrue(n1 == n1)
        self.assertTrue(n1 == n2)
        self.assertFalse(n1 is n2)
        self.assertFalse(n1 == n3)
        self.assertFalse(n1 == n4)

    def test_graph_eq(self):
        g1 = Graph()
        g2 = Graph()
        g3 = Graph()

        g1.add_node(1, 'aaa')
        g1.add_node(2, 'bbb')
        g1.add_node(3, 'ccc')

        g2.add_node(1, 'aaa')
        g2.add_node(2, 'bbb')
        g2.add_node(3, 'ccc')

        g3.add_node(1, 'aaa')
        g3.add_node(2, 'bbb')
        g3.add_node(4, 'ddd')

        self.assertTrue(g1 == g2)
        self.assertFalse(g1 is g2)
        self.assertFalse(g1 == g3)

    def test_node_str(self):
        n1 = Node(1, 'aaa')
        n1.add_child(Node(2, 'bbb'))
        n1.add_child(Node(3, 'c'))

        print(n1)
        self.assertEqual(n1.__str__(), '<Node 1 children: [2, 3]>')


if __name__ == '__main__':
    unittest.main()
