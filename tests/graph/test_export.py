# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context
import os
import unittest
import networkx as nx

from cat2vec.graph.graph import Graph
from cat2vec.graph.export import to_graphml


def _create_graph() -> Graph:
    g = Graph()

    g.add_node(1, 'a')
    g.add_node(2, 'b')
    g.add_node(3, 'c')
    g.add_node(4, 'd')
    g.add_node(5, 'e')
    g.add_node(6, 'f')

    g.add_edges(1, [2, 3])
    g.add_edges(2, [4, 6])
    g.add_edges(3, [4, 5, 6])

    return g


class ExportTest(unittest.TestCase):
    def test_to_graphml(self):
        graph = _create_graph()
        dst_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'tmp_graph.graphml')

        try:
            to_graphml(graph, dst_path)

            self.assertTrue(os.path.exists(dst_path))
            loaded_graph = nx.read_graphml(dst_path)
            self.assertEqual(len(loaded_graph.nodes()), len(graph.nodes.values()))

        finally:
            if os.path.exists(dst_path):
                os.remove(dst_path)


if __name__ == '__main__':
    unittest.main()
