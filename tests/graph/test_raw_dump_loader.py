# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import unittest
import os.path

import cat2vec.graph.raw_dump_loader as loader
from cat2vec.graph.graph import Graph


class RawDumpLoaderTest(unittest.TestCase):
    resources_dir = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')), 'resources', 'wiki-dump')

    def test_categories_loading(self):
        categories_path = os.path.join(RawDumpLoaderTest.resources_dir, 'category.sql')
        graph = loader._load_categories(categories_path)
        self.assertSetEqual(set(graph.nodes.keys()), {2, 3, 7, 8, 9, 10, 11})

        self.assertEqual(graph.nodes[2].number_of_articles, 1392934)
        self.assertEqual(graph.nodes[3].number_of_articles, 88)
        self.assertEqual(graph.nodes[7].number_of_articles, 384)

    def test_pages_ids_loading(self):
        pages_path = os.path.join(RawDumpLoaderTest.resources_dir, 'page.sql')

        graph = Graph()
        graph.add_node(1, 'AccessibleComputing')
        graph.add_node(2, 'AfghanistanHistory')
        graph.add_node(3, 'AmoeboidTaxa')

        page_id2name = loader._load_page_ids(pages_path, graph)

        expected_page_id2name = {
            10: 'AccessibleComputing',
            13: 'AfghanistanHistory',
            24: 'AmoeboidTaxa',
        }
        self.assertDictEqual(page_id2name, expected_page_id2name)

    def test_links_loading(self):
        links_path = os.path.join(RawDumpLoaderTest.resources_dir, 'categorylink.sql')
        links = loader._load_subcategory_to_category_links(links_path)
        self.assertListEqual(links, [(12, 'Anarchism')])

    def test_categories_per_articles(self):
        links_path = os.path.join(RawDumpLoaderTest.resources_dir, 'categorylink.sql')
        pages_links = loader.load_categories_per_article(links_path)
        self.assertDictEqual(pages_links, {10: 3, 12: 2})


if __name__ == '__main__':
    unittest.main()
