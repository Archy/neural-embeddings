# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context
import unittest

from cat2vec.utils.common import _tokenize_category_name


class GraphLoaderTest(unittest.TestCase):
    def test_category_name_tokenization(self):
        category_name = 'Dogs_(_of_the_)Dębica_People\\\'s_(1945–1975)'
        expected_words = ['dog', 'of', 'the', 'dębica', 'people', 's', '1945', '–', '1975']

        words = _tokenize_category_name(category_name)

        self.assertListEqual(words, expected_words)


if __name__ == '__main__':
    unittest.main()
