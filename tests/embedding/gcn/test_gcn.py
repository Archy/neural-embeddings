# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context
import numpy as np
import unittest

from cat2vec.graph.graph import Graph
from cat2vec.embedding.gcn.gcn import _create_cac, _create_target_matrix


def _create_graph() -> Graph:
    g = Graph()

    g.add_node(1, 'a')
    g.add_node(2, 'b')
    g.add_node(3, 'c')
    g.add_node(4, 'd')
    g.add_node(5, 'e')
    g.add_node(6, 'f')

    g.add_edges(1, [2, 3])
    g.add_edges(2, [4, 6])
    g.add_edges(3, [4, 5, 6])

    return g


class GCNTest(unittest.TestCase):
    def test_create_cac(self):
        # given
        graph = _create_graph()
        node2idx = {
            1: 0,
            2: 1,
            3: 2,
            4: 3,
            5: 4,
            6: 5,
        }

        expected_adj_hat = np.array([
            [1, 1, 1, 0, 0, 0],
            [0, 1, 0, 1, 0, 1],
            [0, 0, 1, 1, 1, 1],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 1],
        ])
        expected_c_hat = np.array([
            [1/np.sqrt(3), 0, 0, 0, 0, 0],
            [0, 1/np.sqrt(3), 0, 0, 0, 0],
            [0, 0, 1/np.sqrt(4), 0, 0, 0],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 1],
        ])
        expected_cac = np.matmul(np.matmul(expected_c_hat, expected_adj_hat), expected_c_hat)

        # when
        cac = _create_cac(graph, node2idx)

        # then
        self.assertTrue(np.allclose(cac, expected_cac))

    def test_create_target_matrix(self):
        graph = _create_graph()
        node2idx = {
            1: 0,
            2: 1,
            3: 2,
            4: 3,
            5: 4,
            6: 5,
        }
        expected_labels = np.array([
            [0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0],
            [0, 1, 1, 0, 0, 0],
            [0, 0, 1, 0, 0, 0],
            [0, 1, 1, 0, 0, 0],
        ])

        # when
        lables = _create_target_matrix(graph, node2idx)

        # then
        self.assertTrue(np.array_equal(lables, expected_labels))


if __name__ == '__main__':
    unittest.main()
