# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import random
import unittest
import numpy as np
from cat2vec.graph.graph import Graph
from cat2vec.walks.random_walk import random_walks
from cat2vec.utils.common import create_node_idx_mapping
from cat2vec.embedding.glove.coocurence_matrix import cc_matrix_from_articles_assignment, cc_matrix_from_random_walks


def _mock_graph():
    g = Graph()

    # nodes
    g.add_node(1, 'a')
    g.add_node(2, 'b')
    g.add_node(3, 'c')
    g.add_node(4, 'd')

    # edges
    g.add_edges(1, [2, 3])
    g.add_edges(2, [4])

    # articles
    def add_article(article, nodes):
        for n in nodes:
            g.nodes[n].articles.add(article)

    add_article(11, [1, 2, 3])
    add_article(12, [2, 3, 4])
    add_article(13, [3, 4])
    add_article(14, [4])
    add_article(15, [1, 4])

    return g


class CoocurenceMatrixTest(unittest.TestCase):
    # noinspection PyMethodMayBeStatic
    def test_cc_matrix_from_articles_assignment(self):
        graph = _mock_graph()

        weighted_cc, _ = cc_matrix_from_articles_assignment(graph=graph,
                                                            node2idx=create_node_idx_mapping(graph),
                                                            max_level=3)

        expected_cc = np.array([
            [0., 0.14198735649917169, 0.1678541887420427, 0.1576787084225114],
            [0.07801157731069053, 0., 0.13119931141769536, 0.13119931141769536],
            [0.057556001424696704, 0.09679727054873029, 0., 0.09679727054873029],
            [0.057556001424696704, 0.057556001424696704, 0.09679727054873029, 0.]
        ])
        np.testing.assert_almost_equal(weighted_cc, expected_cc)

    # noinspection PyMethodMayBeStatic
    def test_cc_matrix_from_random_walks(self):
        graph = _mock_graph()
        random.seed(42)

        weighted_cc, _ = cc_matrix_from_random_walks(graph=graph,
                                                     node2idx=create_node_idx_mapping(graph),
                                                     walks=random_walks(graph, walks_per_node=2, walk_length=4),
                                                     window_size=3)
        print(weighted_cc)

        expected_cc = np.array([
            [0., 0.057556, 0.057556, 0.034223],
            [0., 0., 0., 0.13119931],
            [0., 0., 0., 0.],
            [0., 0.01023507, 0., 0.]
        ])
        np.testing.assert_almost_equal(weighted_cc, expected_cc)


if __name__ == '__main__':
    unittest.main()
