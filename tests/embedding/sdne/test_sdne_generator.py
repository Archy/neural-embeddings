# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import unittest
import numpy as np
from cat2vec.embedding.sdne.sdne_generator import SdneGenerator
from cat2vec.graph.graph import Graph
from cat2vec.utils.common import create_node_idx_mapping


def _create_graph() -> Graph:
    g = Graph()

    g.add_node(1, 'a')
    g.add_node(2, 'b')
    g.add_node(3, 'c')
    g.add_node(4, 'd')
    g.add_node(5, 'e')
    g.add_node(6, 'f')

    g.add_edges(1, [2, 3])
    g.add_edges(2, [4, 6])
    g.add_edges(3, [4, 5, 6])

    return g


class SDNEGeneratorTest(unittest.TestCase):
    def test_generator(self):
        np.random.seed(42)
        graph = _create_graph()
        node2idx = create_node_idx_mapping(graph)
        batch_size = 3

        sdne_generator = SdneGenerator(graph, node2idx, batch_size)
        gen = sdne_generator.generator()
        batches = [next(gen) for _ in range(sdne_generator.steps_per_epoch)]

        vectors = np.array([batch[0][0][0] for batch in batches])
        adj_matrices = [batch[0][1][0] for batch in batches]
        target_vectors = [batch[1][1][0] for batch in batches]
        target_empty = [batch[1][0][0] for batch in batches]

        expected_vectors = [
            # batch 1
            [[0, 1, 1, 0, 0, 0],
             [1, 0, 0, 1, 0, 1],
             [1, 0, 0, 1, 1, 1]],
            # batch 2
            [[1, 0, 0, 1, 0, 1],
             [0, 1, 1, 0, 0, 0],
             [0, 1, 1, 0, 0, 0]],
            # batch 3
            [[0, 1, 1, 0, 0, 0],
             [1, 0, 0, 1, 1, 1],
             [0, 1, 1, 0, 0, 0]],
            # batch 4
            [[0, 0, 1, 0, 0, 0],
             [0, 1, 1, 0, 0, 0],
             [0, 0, 1, 0, 0, 0]]
        ]
        self.assertTrue(np.array_equal(vectors, expected_vectors))

        expected_batch_adjacency = [
            # batch 1
            [[0, 1, 1],
             [1, 0, 0],
             [1, 0, 0]],
            # batch 2
            [[0, 1, 1],
             [1, 0, 0],
             [1, 0, 0]],
            # batch 3
            [[0, 1, 0],
             [1, 0, 1],
             [0, 1, 0]],
            # batch 4
            [[0, 0, 0],
             [0, 0, 0],
             [0, 0, 0]],
        ]
        self.assertTrue(np.array_equal(adj_matrices, expected_batch_adjacency))

        self.assertTrue(np.array_equal(vectors, target_vectors))

        self.assertTupleEqual(np.asarray(target_empty).shape, (4, 1, 1))

    def test_embedding_generator(self):
        np.random.seed(42)
        graph = _create_graph()
        node2idx = create_node_idx_mapping(graph)

        sdne_generator = SdneGenerator(graph, node2idx, 1)
        vectors = np.asarray([vector[0] for vector in sdne_generator.embedding_generator()])

        expected_vectors = np.array([
            [0, 1, 1, 0, 0, 0],
            [1, 0, 0, 1, 0, 1],
            [1, 0, 0, 1, 1, 1],
            [0, 1, 1, 0, 0, 0],
            [0, 0, 1, 0, 0, 0],
            [0, 1, 1, 0, 0, 0]
        ])
        self.assertTrue(np.array_equal(vectors, expected_vectors))


if __name__ == '__main__':
    unittest.main()
