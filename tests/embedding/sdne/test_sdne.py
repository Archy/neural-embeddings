# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context
import numpy as np

import unittest
import tensorflow as tf


class SDNETest(unittest.TestCase):
    def test_train_accuracy(self):
        np_y_true = np.array([[
            [1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 0, 1, 1],
            [1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        ]])
        np_y_pred = np.array([[
            [1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
            [1, 0, 1, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
        ]])

        V = 10
        tf_true_input = tf.placeholder(dtype=tf.float32, shape=(1, None, V), name='tf_true')
        tf_pred_input = tf.placeholder(dtype=tf.float32, shape=(1, None, V), name='tf_pred')

        y_true = tf_true_input[0]
        y_pred = tf_pred_input[0]

        tf_true_positives = tf.cast(tf.equal(y_true, y_pred), tf.float32) * y_true
        tf_correct_true_predictions = tf.math.reduce_sum(tf_true_positives, axis=1)

        tf_total_edges = tf.math.reduce_sum(y_true, axis=1)

        tf_per_node_accuracy = tf_correct_true_predictions / tf_total_edges
        tf_mean_accuracy = tf.reduce_mean(tf_per_node_accuracy)

        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init)

            total_true_predicted, total_edges, per_node_accuracy, mean_accuracy = sess.run(
                [tf_correct_true_predictions, tf_total_edges, tf_per_node_accuracy, tf_mean_accuracy],
                feed_dict={
                    tf_true_input: np_y_true,
                    tf_pred_input: np_y_pred
                })

            self.assertTrue(np.allclose(total_true_predicted, 2 * np.ones(4)))
            self.assertTrue(np.allclose(total_edges, 3 * np.ones(4)))
            self.assertTrue(np.allclose(per_node_accuracy, 0.6666667 * np.ones(4)))
            self.assertAlmostEqual(mean_accuracy, 0.6666667)

    def test_l2_loss(self):
        beta = 2
        np_y_true = np.array([
            [1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 0, 1, 1],
            [1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        ])
        np_y_pred = np.array([
            [1, 0, 1, 0, 0, 1, 0, 0, 1, 0],
            [1, 1, 0, 0, 0, 0, 0, 1, 1, 0],
            [1, 0, 1, 1, 0, 0, 0, 0, 0, 0],
            [1, 1, 0, 1, 1, 0, 0, 0, 0, 0],
        ])

        V = 10
        y_true = tf.placeholder(dtype=tf.float32, shape=(None, V), name='tf_true')
        y_pred = tf.placeholder(dtype=tf.float32, shape=(None, V), name='tf_pred')

        B = (y_true * (beta - 1)) + 1
        diff = y_pred - y_true
        node_loss = tf.pow((diff * B), 2)
        loss = tf.math.reduce_sum(node_loss)

        init = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init)

            b_out, diff_out, node_loss_out, loss_out = sess.run(
                [B, diff, node_loss, loss],
                feed_dict={
                    y_true: np_y_true,
                    y_pred: np_y_pred
                })
            self.assertTrue(np.array_equal(diff_out, np_y_pred - np_y_true))


if __name__ == '__main__':
    unittest.main()
