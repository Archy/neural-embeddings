# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import unittest
import huffman
import numpy as np
import cat2vec.embedding.deep_walk.hierarchical_softmax_generator as generator
from cat2vec.graph.graph import Graph


def _create_graph():
    graph = Graph()
    graph.add_node(1, 'cat')
    graph.add_node(3, 'racoon')
    graph.add_node(6, 'dog')
    graph.add_node(11, 'apple')

    freq = {
        1: 10,
        3: 6,
        6: 3,
        11: 1
    }

    return graph, freq


class HierarchicalGeneratorTest(unittest.TestCase):
    def test_create_softmax_tree(self):
        graph, freq = _create_graph()

        node2indices = generator._create_softmax_tree(graph, freq)
        print(node2indices)
        expected = {
            1: (np.asarray([0]), np.asarray([])),
            3: (np.asarray([]), np.asarray([0, 1])),
            6: (np.asarray([1]), np.asarray([0, 2])),
            11: (np.asarray([1, 2]), np.asarray([0]))
        }

        self.assertCountEqual(node2indices, expected)

    def test_huffman2idx(self):
        graph, freq = _create_graph()
        tree = huffman.Tree(freq.items())

        huff2idx = generator._huffman2idx(tree)
        expected = {
            '': 0,
            '1': 1,
            '10': 2
        }

        self.assertDictEqual(huff2idx, expected)

    def test_pad(self):
        indicies_1 = np.asarray([1, 2, 3])
        indicies_2 = np.asarray([0])
        indicies_3 = np.asarray([1, 2, 3, 6, 7, 8, 9, 0])
        indicies_4 = np.asarray([5, 9, 0])
        indicies_5 = np.asarray([1, 1, 2, 6, 7])

        seq = [indicies_1, indicies_2, indicies_3, indicies_4, indicies_5]

        padded, mask = generator._pad(seq)
        self.assertTrue(np.array_equal(padded, np.asarray([
            [1, 2, 3, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [1, 2, 3, 6, 7, 8, 9, 0],
            [5, 9, 0, 0, 0, 0, 0, 0],
            [1, 1, 2, 6, 7, 0, 0, 0],
        ])))
        self.assertTrue(np.allclose(mask, np.asarray([
            [1, 1, 1, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 0, 0, 0],
        ])[:, :, np.newaxis]))


if __name__ == '__main__':
    unittest.main()
