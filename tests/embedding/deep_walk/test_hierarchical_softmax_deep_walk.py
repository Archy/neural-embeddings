# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import unittest
import numpy as np
from cat2vec.embedding.deep_walk.hierarchical_softmax_deep_walk import DeepWalk


class DeepWalkTest(unittest.TestCase):
    def test_einsum(self):
        batches = 3
        indices = 10
        embedding_size = 5

        indices_tensor = np.random.rand(batches, indices, embedding_size)
        input_tensor = np.random.rand(batches, 1, embedding_size)

        einsum = np.einsum(DeepWalk.EINSUM_EQUATION, indices_tensor, input_tensor)
        self.assertTrue(einsum.shape == (batches, indices))

        for sample in range(batches):
            input_v = input_tensor[sample, 0]
            for index in range(indices):
                idx_v = indices_tensor[sample, index]

                expected = idx_v.dot(input_v)
                self.assertAlmostEqual(einsum[sample, index], expected)


if __name__ == '__main__':
    unittest.main()
