# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import unittest
import numpy as np
from cat2vec.graph.graph import Graph
from cat2vec.utils.common import create_node_idx_mapping
from cat2vec.embedding.deep_walk.negative_samples_generator import NegativeSamplesGenerator


def mock_generator(window_size=3, negative_samples_num=5):
    np.random.seed(42)

    graph = Graph()
    graph.add_node(1, 'cat')
    graph.add_node(2, 'racoon')
    graph.add_node(3, 'dog')
    graph.add_node(4, 'apple')
    graph.add_node(5, 'banana')

    gen = NegativeSamplesGenerator(
        graph=graph,
        node2idx=create_node_idx_mapping(graph),
        walks=[],
        window_size=window_size,
        negative_samples_num=negative_samples_num,
        batch_size=0
    )

    return gen


class NegativeSamplesGeneratorTest(unittest.TestCase):
    def test__create_pos_samples(self):
        generator = mock_generator()
        walk = [1, 2, 3, 4, 5]

        inputs, targets = generator._create_pos_samples(walk)
        self.assertListEqual(inputs, [1, 2, 2, 3, 3, 3, 4, 4, 4])
        self.assertListEqual(targets, [0, 0, 1, 0, 1, 2, 1, 2, 3])

    def test__create_neg_samples(self):
        generator = mock_generator()
        walk = [1, 2, 3, 4, 5]

        neg_inputs, neg_targets = generator._create_neg_samples(walk)
        self.assertListEqual(neg_inputs, [1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4])
        self.assertListEqual(neg_targets, [4, 2, 4, 4, 2, 3, 3, 4, 3, 3, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0])


if __name__ == '__main__':
    unittest.main()
