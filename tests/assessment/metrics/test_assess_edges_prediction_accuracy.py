# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context
from cat2vec.assessment.metrics.edges_prediction_accuracy import _count_accuracy

import numpy as np
import unittest


class EdgesPredictionTest(unittest.TestCase):
    def test_count_accuracy(self):
        # given
        pos_score = np.array([1, 0, 0, 1, 0])
        pos_labels = np.ones((5,))
        neg_score = np.array([0, 0, 0, 1, 0])
        neg_labels = np.zeros((5,))

        # when
        results = _count_accuracy(pos_score, pos_labels, neg_score, neg_labels)

        # then
        self.assertEqual(results['True positive'], '2 (0.4)')
        self.assertEqual(results['False negative'], '3 (0.6)')
        self.assertEqual(results['True negative'], '4 (0.8)')
        self.assertEqual(results['False positive'], '1 (0.2)')
        self.assertAlmostEqual(results['Precision'], 0.67, places=2)
        self.assertAlmostEqual(results['Recall'], 0.4, places=2)
        self.assertAlmostEqual(results['F1'], 0.5, places=2)


if __name__ == '__main__':
    unittest.main()
