# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

from cat2vec.assessment.metrics.neighbours_coverage import assess_neighbours_coverage
from cat2vec.graph.graph import Graph

from unittest.mock import patch
import unittest


def _create_graph() -> Graph:
    g = Graph()

    g.add_node(1, 'a')
    g.add_node(2, 'b')
    g.add_node(3, 'c')
    g.add_node(4, 'd')
    g.add_node(5, 'e')
    g.add_node(6, 'f')

    g.add_edges(1, [2, 3])
    g.add_edges(2, [4, 6])
    g.add_edges(3, [4, 5, 6])

    return g


def _predict_neighbours(idx, links, metric):
    predicted = []
    if metric == 'cosine':
        if idx == 1:
            predicted = [2, 3]
        elif idx == 2:
            predicted = [4, 5]
        elif idx == 3:
            predicted = [6, 1, 2]
    elif metric == 'euclidean':
        if idx == 1:
            predicted = [2, 6]
        elif idx == 2:
            predicted = [4, 6]
        elif idx == 3:
            predicted = [4, 6, 1]
    else:
        raise Exception('Wrong metric')

    assert len(predicted) == links
    return predicted


class NeighboursCoverageTest(unittest.TestCase):
    def test_assess(self):
        # given
        with patch('cat2vec.embedding.embedding_model.EmbeddingModel', autospec=True) as model:
            model.predict.side_effect = _predict_neighbours
            graph = _create_graph()

            # when
            results = assess_neighbours_coverage(graph, model)

            # then

            cosine_results = results['cosine']
            self.assertEqual(cosine_results['Total neighbours'], 7)
            self.assertEqual(cosine_results['Predicted neighbours'], 4)
            self.assertAlmostEqual(cosine_results['Prediction mean'], 0.611, places=3)
            self.assertAlmostEqual(cosine_results['Prediction median'], 0.500, places=3)
            self.assertAlmostEqual(cosine_results['Prediction std dev'], 0.347, places=3)

            euclidean_results = results['euclidean']
            self.assertEqual(euclidean_results['Total neighbours'], 7)
            self.assertEqual(euclidean_results['Predicted neighbours'], 5)
            self.assertAlmostEqual(euclidean_results['Prediction mean'], 0.722, places=3)
            self.assertAlmostEqual(euclidean_results['Prediction median'], 0.667, places=3)
            self.assertAlmostEqual(euclidean_results['Prediction std dev'], 0.255, places=3)


if __name__ == '__main__':
    unittest.main()
