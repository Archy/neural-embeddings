# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

from unittest.mock import patch

import unittest
from cat2vec.assessment.embeddings_assessor import _WalksMemento


class EmbeddingsAssessorTest(unittest.TestCase):
    # noinspection PyMethodMayBeStatic
    def test_no_walks(self):
        with patch('cat2vec.graph.graph.Graph', autospec=True) as graph_mock, patch(
                'cat2vec.walks.random_walk.random_walks') as random_walks:
            memento = _WalksMemento(graph_mock)
            memento.get_random_walks(sth=123)
            random_walks.assert_not_called()

    # noinspection PyMethodMayBeStatic
    def test_update_needed(self):
        with patch('cat2vec.graph.graph.Graph', autospec=True) as graph_mock, patch(
                'cat2vec.walks.random_walk.random_walks') as random_walks:
            memento = _WalksMemento(graph_mock)

            # first walks generation
            memento.get_random_walks(walks_per_node=2, walk_length=5, restart_probability=0.5)
            random_walks.assert_called_once()
            random_walks.reset_mock()

            # config hasn't changed - no walks generation
            memento.get_random_walks(walks_per_node=2, walk_length=5, restart_probability=0.5)
            random_walks.assert_not_called()
            random_walks.reset_mock()

            # config changed - generate new walks
            memento.get_random_walks(walks_per_node=3, walk_length=5, restart_probability=0.5)
            random_walks.assert_called_once()

    def test_invalid_config(self):
        with patch('cat2vec.graph.graph.Graph', autospec=True) as graph_mock:
            memento = _WalksMemento(graph_mock)
            self.assertRaises(AssertionError, memento.get_random_walks, walks_per_node=3, restart_probability=0.5)


if __name__ == '__main__':
    unittest.main()
