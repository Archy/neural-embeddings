# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

import os

from cat2vec.assessment.assessment_saver.local_assessment_saver import LocalAssessmentSaver

from unittest.mock import patch, mock_open, call, ANY
import unittest


class EmbeddingsAssessorTest(unittest.TestCase):
    def test_save(self):
        with patch('cat2vec.embedding.embedding_model.EmbeddingModel', autospec=True) as model, \
                patch('builtins.open', mock_open(read_data='some stuff')) as mock_file_open, \
                patch('os.path.exists', return_value=False), \
                patch('json.dump') as json_dump, \
                patch('csv.writer') as csv_writer:
            saver = LocalAssessmentSaver('test.csv', 'data_dir')
            self.assertEqual(saver.next_id, 1)

            assessment_results = {'metric': {'result': 1}}
            model.get_name.return_value = 'model_v1'

            saver.save(model, assessment_results)

            model.save.assert_called_once_with('data_dir', 'model_v1.1')
            json_dump.assert_called_once()
            csv_writer().writerow.assert_called_once_with([1, 'model_v1', 'model_v1.1', ANY])

            mock_file_open.assert_has_calls([
                call(os.path.join('data_dir', 'model_v1.1.data.json'), mode='w'),
                call().__enter__(),
                call().__exit__(None, None, None),
                call('test.csv', mode='a'),
                call().__enter__(),
                call().__exit__(None, None, None)
            ])

            self.assertEqual(saver.next_id, 2)


if __name__ == '__main__':
    unittest.main()
