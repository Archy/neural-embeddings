# hack for importing from sibling dirs
# noinspection PyUnresolvedReferences
from tests import context

from unittest.mock import Mock, patch

import unittest
from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, Metrics, AssessmentConfig


class EmbeddingsAssessorTest(unittest.TestCase):
    # noinspection PyMethodMayBeStatic
    def test_fit(self):
        with patch('cat2vec.embedding.embedding_model.EmbeddingModel', autospec=True) as model, \
                patch('cat2vec.assessment.assessment_saver.assessment_saver.AssessmentSaver', autospec=True) as saver, \
                patch('cat2vec.assessment.embeddings_assessor._WalksMemento') as memento, \
                patch('cat2vec.graph.loader.load_graph', return_value='graph') as graph_loader:
            assessor = EmbeddingsAssessor(model, saver, 'cat_path', 'dict_path')
            graph_loader.assert_called_once()
            memento.return_value.get_random_walks.return_value = 'walks'
            assessor.fit(learning_rate=0.1, epochs=100, arg1='aa', arg2='bb')
            model.fit_args.assert_called_once_with('graph', 0.1, 100, walks='walks', arg1='aa', arg2='bb')

    # noinspection PyMethodMayBeStatic
    def test_assess(self):
        with patch('cat2vec.embedding.embedding_model.EmbeddingModel', autospec=True) as model, \
                patch('cat2vec.assessment.assessment_saver.assessment_saver.AssessmentSaver', autospec=True) as saver, \
                patch('cat2vec.graph.loader.load_graph', return_value='graph'):
            assessor = EmbeddingsAssessor(model, saver, 'cat_path', 'dict_path')

            neighbours_coverage = Mock()
            edges_prediction = Mock()
            closest_neighbours = Mock()
            tsne_visualization = Mock()
            dict_mock = {
                Metrics.NEIGHBOURS_COVERAGE: neighbours_coverage,
                Metrics.EDGES_PREDICTION: edges_prediction,
                Metrics.PRINT_NEIGHBOURS: closest_neighbours,
                Metrics.TSNE_VISUALIZATION: tsne_visualization
            }
            assessor.assessment_dict = dict_mock

            metrics = AssessmentConfig() \
                .with_edges_prediction(negative_edges_per_node=5) \
                .with_neighbours_coverage() \
                .with_closest_neighbours(test_no=2, neighbors_no=5) \
                .with_tsne_visualization(image_size=(40, 20), dpi=300, dst_path='dst.png')

            assessor.assess_model(save_results=False, config=metrics)
            neighbours_coverage.assert_called_once()
            edges_prediction.assert_called_once()
            closest_neighbours.assert_called_once()
            tsne_visualization.assert_called_once()

            neighbours_coverage.reset_mock()
            edges_prediction.reset_mock()
            closest_neighbours.reset_mock()
            tsne_visualization.reset_mock()

            metrics.clear()\
                .with_neighbours_coverage()\
                .with_tsne_visualization(image_size=(40, 20), dpi=300, dst_path='dst.png')

            assessor.assess_model(save_results=False, config=metrics)
            neighbours_coverage.assert_called_once()
            edges_prediction.assert_not_called()
            closest_neighbours.assert_not_called()
            tsne_visualization.assert_called_once()

    def test_print_closest_neighbours(self):
        pass


if __name__ == '__main__':
    unittest.main()
