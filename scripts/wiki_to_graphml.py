from graph import loader
from graph.export import to_graphml

CAT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'

DST_PATH = r'../wiki-graph/hist_of_poland-10h/history-of-poland.graphml'


def convert_wiki_to_graphml():
    graph = loader.load_graph(CAT_PATH, DICT_PATH, root='History_of_Poland')
    # to_graphml(graph, DST_PATH)


if __name__ == '__main__':
    convert_wiki_to_graphml()
