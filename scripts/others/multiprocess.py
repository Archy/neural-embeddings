import concurrent
import random
from concurrent.futures import as_completed
import multiprocessing
import os
import time
from queue import Empty


def process(prime, counter):
    time.sleep(1)
    print(os.getpid(), ' - ', prime)

    running = True
    while running:
        with counter.get_lock():
            counter.value += 1
            last_val = counter.value
        if last_val > 1_000_000_000:
            print(os.getpid(), ' finishing')
            running = False

    return os.getpid()


def worker_main(queue):
    print(os.getpid(), " working")

    try:
        while True:
            item = queue.get_nowait()
            print(os.getpid(), " got ", item)
            time.sleep(2)  # simulate a "long" operation
    except Empty:
        print(os.getpid(), " finished")
    return os.getpid()


class A:

    def __init__(self) -> None:
        self.tab = [
            112272535095293,
            112582705942171,
            112272535095293,
            115280095190773,
            115797848077099,
            1099726899285419]

    def run(self):
        the_queue = multiprocessing.Queue()
        for prime in self.tab:
            the_queue.put(prime)

        # (the_queue.put(i) for i in self.tab)

        the_pool = multiprocessing.Pool(3, worker_main, (the_queue,))
        the_pool.close()
        the_pool.join()


    def run2(self):
        results = []
        counter = multiprocessing.Manager().Value('d', 0)

        with concurrent.futures.ProcessPoolExecutor(max_workers=os.cpu_count()) as executor:
            for prime in self.tab:
                results.append(executor.submit(process, prime, counter))

            for complete in as_completed(results):
                print(complete.result())


if __name__=='__main__':
    a = A()
    a.run()
