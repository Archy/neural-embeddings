import numpy as np
import tensorflow as tf


def test_const():
    a = tf.Variable(np.array([1, 2]))
    op = 2 * a

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        print(sess.run(op))


def test_multiply():
    tesnors = np.asarray([
        # sample 1
        [
            [1, 2, 3],
            [5, 5, 6],
            [7, 8, 9],
        ],
        # sample 2
        [
            [1, 2, 3],
            [5, 5, 6],
            [-1, -1, -1],   # vertices to mask
        ]
    ])
    print(tesnors.shape)

    mask = np.asarray([
        [
            [1],
            [1],
            [1],
        ],
        [
            [1],
            [1],
            [0],
        ]
    ])
    print(mask.shape)

    tf_tensors = tf.Variable(tesnors)
    tf_mask = tf.Variable(mask)
    op = tf.multiply(tf_tensors, tf_mask)
    #
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        print(sess.run(op))


def test_update():
    # numpy
    a = np.array([[1, 1, 0],
                  [1, 1, 0],
                  [0, 0, 1]])
    c = np.array([[2, 0, 0],
                  [0, 2, 0],
                  [0, 0, 1]])
    expected = np.dot(a, c)

    # tensorflow
    tfA = tf.Variable(a)
    tfC = tf.Variable(c)

    const = tf.matmul(tfA, tfC)

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        print(sess.run(const))


if __name__ == '__main__':
    # test_const()
    # test_multiply()
    test_update()
