#hack for importing from sibling dirs
import context
from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.sdne.sdne import SDNE

import logging.config
logging.config.fileConfig('logging.ini', disable_existing_loggers=False)



# load full graph
FULL_CAT_PATH = r'D:\Datasets\enwiki-20190601-category.sql'
FULL_PAGE_PATH = r'D:\Datasets\enwiki-20190601-page.sql'
FULL_LINKS_PATH = r'D:\Datasets\enwiki-20190601-categorylinks.sql'

print('loading_full_graph')
from cat2vec.graph.raw_dump_loader import load_graph
graph = load_graph(FULL_CAT_PATH, FULL_PAGE_PATH, FULL_LINKS_PATH)
print('full_graph_loaded')

# history-of-poland (depth 10) -> ~5325 nodes
CAT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'

model = SDNE(embedding_size=300, additional_hidden_layers_shapes=[5000])

assessor = EmbeddingsAssessor(model=model,
                              assessment_saver=None,
                              cat_path=CAT_PATH,
                              dict_path=DICT_PATH,
                              root_node='History_of_Poland')

assessor.graph = graph


assessor.fit(epochs=1,
                 learning_rate=0.000018,  # 0.000024 00000417
                 batch_size=32,
                 regularization=0.1,
                 beta=50,
                 loss1_weight=0.20,
                 loss2_weight=1)