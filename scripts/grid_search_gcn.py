import logging.config

from cat2vec.assessment.assessment_saver.local_assessment_saver import LocalAssessmentSaver
from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.gcn.gcn import GCN
from itertools import product

CAT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'


def test_architecture():
    logger = logging.getLogger(__name__)

    layers_shapes_values = [
        [50, 100],
        [50, 300],
        [100, 100],
        [100, 300],
    ]
    beta_values = [5, 10, 50]
    learning_rate_values = [0.00821, 0.00121, 0.00013, 0.000076]

    total = 0
    for layers_shape, beta, learning_rate in product(
            layers_shapes_values,
            beta_values,
            learning_rate_values):
        total += 1

        try:
            logger.info('Layers: %s, beta: %s, learning_rate rate: %f', str(layers_shape), beta, learning_rate)

            model = GCN(layers_shapes=layers_shape)
            saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                         dst_path=r'D:\cat2vec\gcn\results')

            assessor = EmbeddingsAssessor(model=model,
                                          assessment_saver=saver,
                                          cat_path=CAT_PATH,
                                          dict_path=DICT_PATH,
                                          root_node='History_of_Poland')

            assessor.fit(epochs=20000,
                         learning_rate=learning_rate,
                         input_features=50,
                         regularization=0.1,
                         beta=beta)

            metrics = AssessmentConfig() \
                .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
                .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128) \
                .with_neighbours_coverage()
            results = assessor.assess_model(save_results=True, config=metrics)
            logger.info(results)

        except Exception as e:
            logger.error(e)

    logger.info('Total: %d', total)


def test_erroneous_edges():
    model = GCN(layers_shapes=[50, 300])

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'D:\cat2vec\gcn\results')

    metrics = AssessmentConfig() \
        .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=True, edges_to_add=500,
                               edges_to_remove=500)

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=saver,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')

    assessor.prefit_assessment_preparation(metrics)

    assessor.fit(epochs=20000,
                 learning_rate=0.00821,
                 input_features=50,
                 regularization=0.1,
                 beta=50)

    results = assessor.assess_model(save_results=True, config=metrics)
    print(results)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    # test_architecture()
    test_erroneous_edges()
