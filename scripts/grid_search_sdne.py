import logging.config

from cat2vec.assessment.assessment_saver.local_assessment_saver import LocalAssessmentSaver
from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.sdne.sdne import SDNE
from itertools import product

CAT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'


def assess_sdne():
    logger = logging.getLogger(__name__)

    model = SDNE(embedding_size=300,  additional_hidden_layers_shapes=[500], use_leaky_relu=True)

    metrics = AssessmentConfig() \
        .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
        .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128) \
        .with_neighbours_coverage()

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\data')

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=saver,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')

    assessor.fit(epochs=100,
                 learning_rate=0.000018,  # 0.000024 00000417
                 batch_size=32,
                 regularization=0.1,
                 beta=50,
                 loss1_weight=0.20,
                 loss2_weight=1)

    results = assessor.assess_model(save_results=False, config=metrics)
    logger.info(results)


def test_architecture():
    logger = logging.getLogger(__name__)

    embedding_size_values = {
        100,
        300
    }
    layers_shapes_values = [
        None,
        [500],
        [1000],
    ]
    learning_rate_values = [0.000024, 0.0000095]
    loss1_weight_values = [0.33, 0.20]

    total = 0
    for embedding_size, layers_shape, learning_rate, loss1_weight in product(
            embedding_size_values,
            layers_shapes_values,
            learning_rate_values,
            loss1_weight_values):
        total += 1
        try:
            logger.info('Embedding size: %d, layers: %s, learning rate: %f, loss1 weight: %f',
                        embedding_size, layers_shape, learning_rate, loss1_weight)
            model = SDNE(embedding_size=embedding_size, additional_hidden_layers_shapes=layers_shape)

            metrics = AssessmentConfig() \
                .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
                .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128) \
                .with_neighbours_coverage()

            saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                         dst_path=r'D:\cat2vec\sdne\results')

            assessor = EmbeddingsAssessor(model=model,
                                          assessment_saver=saver,
                                          cat_path=CAT_PATH,
                                          dict_path=DICT_PATH,
                                          root_node='History_of_Poland')

            assessor.fit(epochs=200,
                         learning_rate=learning_rate,
                         batch_size=32,
                         regularization=0.1,
                         beta=50,
                         loss1_weight=loss1_weight,
                         loss2_weight=1)
            results = assessor.assess_model(save_results=True, config=metrics)
            logger.info(results)

        except Exception as e:
            logger.error(e)

    logger.info('Total: %d', total)


def test_erroneous_edges():
    model = SDNE(embedding_size=300,  additional_hidden_layers_shapes=[500], use_leaky_relu=True)

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'D:\cat2vec\sdne\results')

    metrics = AssessmentConfig() \
        .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=True, edges_to_add=500,
                               edges_to_remove=500)

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=saver,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')

    assessor.prefit_assessment_preparation(metrics)

    # assessor.fit(epochs=200,
    #              learning_rate=0.000024,
    #              batch_size=32,
    #              regularization=0.1,
    #              beta=50,
    #              loss1_weight=0.2,
    #              loss2_weight=1)
    assessor.load_model(r'D:\cat2vec\sdne\results', 'sdne_v1.281')

    results = assessor.assess_model(save_results=True, config=metrics)
    print(results)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    # assess_sdne()
    # test_architecture()
    test_erroneous_edges()
