import logging.config

from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.glove.glove import Glove


CAT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'
SRC_PATH = r'D:\cat2vec\glove\data'


def retest_model(embedding_size: int, model_prefix: str) -> None:
    metrics = AssessmentConfig()\
        .with_neighbours_coverage()
    model = Glove(embedding_size=embedding_size)

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=None,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')
    assessor.load_model(SRC_PATH, model_prefix)

    assessor.assess_model(save_results=False, config=metrics)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    retest_model(50, 'glove_v1.70')
    retest_model(100, 'glove_v1.76')
    retest_model(200, 'glove_v1.78')
    retest_model(300, 'glove_v1.82')
