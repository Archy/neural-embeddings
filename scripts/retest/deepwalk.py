import logging.config

from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.deep_walk.negative_sampling_deep_walk import DeepWalk

CAT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'
SRC_PATH = r'D:\cat2vec\deepwalk\negative sampling\grid search\data'


def retest_model(embedding_size: int, model_prefix: str) -> None:
    metrics = AssessmentConfig()\
        .with_neighbours_coverage()
    model = DeepWalk(embedding_size=embedding_size)

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=None,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')
    assessor.load_model(SRC_PATH, model_prefix)

    assessor.assess_model(save_results=False, config=metrics)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    retest_model(50, 'deepWalk_negativeSampling_v1.159')
    retest_model(100, 'deepWalk_negativeSampling_v1.164')
    retest_model(200, 'deepWalk_negativeSampling_v1.209')
    retest_model(300, 'deepWalk_negativeSampling_v1.204')
