import logging.config
from typing import List

from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.sdne.sdne import SDNE

CAT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'
SRC_PATH = r'D:\cat2vec\sdne\results'


def retest_model(embedding_size: int, hidden_layers: List[int], model_prefix: str) -> None:
    metrics = AssessmentConfig()\
        .with_neighbours_coverage()
    model = SDNE(embedding_size=embedding_size,
                 additional_hidden_layers_shapes=hidden_layers,
                 use_leaky_relu=True)

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=None,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')
    assessor.load_model(SRC_PATH, model_prefix)

    assessor.assess_model(save_results=False, config=metrics)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    retest_model(100, None, 'sdne_v1.233')
    retest_model(300, None, 'sdne_v1.259')
    retest_model(100, [500], 'sdne_v1.243')
    retest_model(300, [500], 'sdne_v1.267')
    retest_model(100, [1000], 'sdne_v1.251')
    retest_model(300, [1000], 'sdne_v1.275')
