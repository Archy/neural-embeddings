import logging.config
from typing import List

from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.gcn.gcn import GCN

CAT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'
SRC_PATH = r'D:\cat2vec\gcn\results'


def retest_model(layers_shapes: List[int], model_prefix: str) -> None:
    metrics = AssessmentConfig()\
        .with_neighbours_coverage()
    model = GCN(layers_shapes=layers_shapes)

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=None,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')
    assessor.load_model(SRC_PATH, model_prefix)

    assessor.assess_model(save_results=False, config=metrics)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    retest_model([50, 100], 'gcn_v1.285')
    retest_model([50, 300], 'gcn_v1.325')
    retest_model([100, 100], 'gcn_v1.341')
    retest_model([100, 300], 'gcn_v1.379')
