import os
import json


def find_model(src_path, results):
    for filename in os.listdir(src_path):
        if filename.endswith('data.json'):
            with open(os.path.join(src_path, filename)) as json_file:
                json_content = json.loads(json_file.read())
                results_map = json_content['assessment_results']['EDGES_PREDICTION']
                if get_result(results_map['AVERAGE']) == results[0] and \
                        get_result(results_map['HADAMARD']) == results[1]:
                    print(filename)
                    break


def get_result(results_map):
    true_positive = int(results_map['True positive'].split(' ')[0])
    true_negatvie = int(results_map['True negative'].split(' ')[0])

    return true_positive, true_negatvie


def find_glove():
    # find saved glove model based on edges reconstruction results
    src_path = r'D:\cat2vec\glove\data'
    # find_model(((378, 13119), (1908, 13119)))
    find_model(src_path, ((354, 13191), (2333, 13062)))
    find_model(src_path, ((413, 13157), (2294, 13172)))
    find_model(src_path, ((507, 13074), (2211, 13254)))
    find_model(src_path, ((504, 13076), (1991, 13272)))


def find_deepwalk():
    # find saved glove model based on edges reconstruction results
    src_path = r'D:\cat2vec\deepwalk\negative sampling\grid search\data'
    find_model(src_path, ((16, 13311), (3161, 13035)))
    find_model(src_path, ((118, 13271), (3306, 13098)))
    find_model(src_path, ((169, 13276), (3279, 13139)))
    find_model(src_path, ((259, 13192), (3304, 13139)))


def find_sdne():
    # find saved glove model based on edges reconstruction results
    src_path = r'D:\cat2vec\sdne\results'
    find_model(src_path, ((575, 13076), (3202, 12930)))
    find_model(src_path, ((669, 13044), (3404, 13072)))
    find_model(src_path, ((711, 13094), (3678, 13120)))
    find_model(src_path, ((526, 13176), (3716, 13144)))
    find_model(src_path, ((718, 13059), (3689, 13096)))
    find_model(src_path, ((640, 13115), (3699, 13130)))


def find_gcn():
    # find saved glove model based on edges reconstruction results
    src_path = r'D:\cat2vec\gcn\results'
    find_model(src_path, ((812, 12673), (3097, 11812)))
    find_model(src_path, ((1059, 12436), (3246, 12385)))
    find_model(src_path, ((808, 12637), (3040, 11749)))
    find_model(src_path, ((1005, 12530), (3244, 12377)))


if __name__ == '__main__':
    # find_glove()
    # find_deepwalk()
    # find_sdne()
    find_gcn()
