import logging.config

from cat2vec.assessment.assessment_saver.local_assessment_saver import LocalAssessmentSaver
from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from cat2vec.embedding.glove.glove import Glove

CAT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'
ART_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-categories-en-20111201'


def test_glove():
    sizes = [50, 100, 200, 300]
    learning_rates = [0.003, 0.00012, 0.000047, 0.000097]

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\data')

    metrics = AssessmentConfig() \
        .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
        .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128) \
        .with_ancestor_prediction(samples_per_node=5, max_distance=2, k=5)

    for size in sizes:
        model = Glove(embedding_size=size)

        assessor = EmbeddingsAssessor(model=model,
                                      assessment_saver=saver,
                                      cat_path=CAT_PATH,
                                      dict_path=DICT_PATH,
                                      art_filepath=ART_PATH,
                                      root_node='History_of_Poland')

        for lr in learning_rates:
            assessor.fit(epochs=3000,
                         learning_rate=lr,
                         reg=0.01,
                         max_level=2)
            assessor.assess_model(save_results=True, config=metrics)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    test_glove()
