import logging.config
from itertools import product

from cat2vec.assessment.assessment_saver.local_assessment_saver import LocalAssessmentSaver
from cat2vec.assessment.embeddings_assessor import EmbeddingsAssessor, AssessmentConfig
from embedding.deep_walk.negative_sampling_deep_walk import DeepWalk

CAT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-cats_links-en-20111201'
DICT_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-cats_dict-en-20111201'
ART_PATH = '../wiki-graph/hist_of_poland-10h/history-of-poland-10-po_linkach-categories-en-20111201'


def test_walks():
    walks_per_node_values = [5, 10, 15]
    walks_lenght_values = [5, 10, 15]

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\data')

    logger = logging.getLogger(__name__)
    logger.info('Doing grid search for walks count and length')

    for walks_per_node, walks_lenggth in product(walks_per_node_values, walks_lenght_values):
        logger.info('CONFIG: %d %d', walks_per_node, walks_lenggth)

        model = DeepWalk(embedding_size=100)
        assessor = EmbeddingsAssessor(model=model,
                                      assessment_saver=saver,
                                      cat_path=CAT_PATH,
                                      dict_path=DICT_PATH,
                                      root_node='History_of_Poland')
        assessor.fit(epochs=70,
                     batch_size=128,
                     learning_rate=0.0072,
                     window_size=8,
                     negative_samples_num=10,
                     use_glove_init=True,
                     walks_per_node=walks_per_node,
                     walk_length=walks_lenggth,
                     restart_probability=0.1)

        metrics = AssessmentConfig() \
            .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
            .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128) \
            .with_ancestor_prediction(samples_per_node=5, max_distance=2, k=5)
        results = assessor.assess_model(save_results=True, config=metrics)
        logger.info(results)


def test_ns():
    window_size_values = [1, 5, 8]
    negative_samples_values = [1, 5]

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\data')

    logger = logging.getLogger(__name__)
    logger.info('Doing grid search for window size and negative samples number')

    for window_size, negative_samples in product(window_size_values, negative_samples_values):
        logger.info('CONFIG (windows, ns_count): %d %d', window_size, negative_samples)

        model = DeepWalk(embedding_size=100)
        assessor = EmbeddingsAssessor(model=model,
                                      assessment_saver=saver,
                                      cat_path=CAT_PATH,
                                      dict_path=DICT_PATH,
                                      root_node='History_of_Poland')
        assessor.fit(epochs=70,
                     batch_size=128,
                     learning_rate=0.0072,
                     window_size=window_size,
                     negative_samples_num=negative_samples,
                     use_glove_init=True,
                     walks_per_node=15,
                     walk_length=10,
                     restart_probability=0.1)

        metrics = AssessmentConfig() \
            .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
            .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128) \
            .with_neighbours_coverage()
        results = assessor.assess_model(save_results=True, config=metrics)
        logger.info(results)


def test_sizes():
    # sizes = [50, 100, 200, 300]
    # learning_rates = [0.0072, 0.0024, 0.00089, 0.00012]
    # sizes = [200, 300]
    # learning_rates = [0.0032, 0.0011, 0.00074]
    # sizes = [300]
    # learning_rates = [0.0032, 0.0019]
    sizes = [200]
    learning_rates = [0.0019]

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\data')

    logger = logging.getLogger(__name__)
    logger.info('Doing grid search for window size and negative samples number')

    for size, learning_rate in product(sizes, learning_rates):
        logger.info('CONFIG (size, learning_rate): %d %f', size, learning_rate)

        model = DeepWalk(embedding_size=size)
        assessor = EmbeddingsAssessor(model=model,
                                      assessment_saver=saver,
                                      cat_path=CAT_PATH,
                                      dict_path=DICT_PATH,
                                      root_node='History_of_Poland')
        assessor.fit(epochs=200,
                     batch_size=128,
                     learning_rate=learning_rate,
                     window_size=8,
                     negative_samples_num=10,
                     use_glove_init=True,
                     walks_per_node=15,
                     walk_length=10,
                     restart_probability=0.1)

        metrics = AssessmentConfig() \
            .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
            .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128) \
            .with_neighbours_coverage()
        results = assessor.assess_model(save_results=True, config=metrics)
        logger.info(results)


def test_deepwalk():
    model = DeepWalk(embedding_size=100)

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\data')

    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=saver,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')
    assessor.fit(epochs=50,
                 batch_size=128,
                 learning_rate=0.0097,
                 window_size=8,
                 negative_samples_num=10,
                 use_glove_init=True,
                 walks_per_node=15,
                 walk_length=5,
                 restart_probability=0.1)
    metrics = AssessmentConfig() \
        .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False) \
        .with_neighbours_coverage() \
        .with_softmax_ancestor_prediction(epochs=20, learning_rate=0.002, batch_size=128)
    results = assessor.assess_model(save_results=False, config=metrics)
    print(results)


def test_erroneous_edges():
    model = DeepWalk(embedding_size=100)

    saver = LocalAssessmentSaver(cvs_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\results.csv',
                                 dst_path=r'C:\Users\Jan\Desktop\neural-embeddings\tmp\data')

    metrics = AssessmentConfig() \
        .with_edges_prediction(negative_edges_per_node=5, print_erroneous_edges=False, edges_to_add=500,
                               edges_to_remove=500)
    assessor = EmbeddingsAssessor(model=model,
                                  assessment_saver=saver,
                                  cat_path=CAT_PATH,
                                  dict_path=DICT_PATH,
                                  root_node='History_of_Poland')

    assessor.prefit_assessment_preparation(metrics)

    assessor.fit(epochs=100,
                 batch_size=128,
                 learning_rate=0.0024,
                 window_size=8,
                 negative_samples_num=10,
                 use_glove_init=True,
                 walks_per_node=15,
                 walk_length=10,
                 restart_probability=0.1)

    results = assessor.assess_model(save_results=True, config=metrics)
    print(results)


if __name__ == '__main__':
    logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
    # test_deepwalk()
    # test_walks()
    # test_ns()
    # test_sizes()
    test_erroneous_edges()
