import nltk

# for tokenizing:
nltk.download('punkt')
# for part of speech (pos) tagging
nltk.download('averaged_perceptron_tagger')
# for lemmatazing:
nltk.download('wordnet')
