import json
import logging
import os
import time
from functools import wraps
from typing import List, Dict

import numpy as np
import tensorflow as tf
from sklearn.metrics import pairwise_distances

from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.graph.graph import Graph
from cat2vec.utils.common import create_node_idx_mapping, create_initial_weight


def train_wrapper(train_method):
    """
    Decorator that saves training config to dictionary
    """

    @wraps(train_method)
    def _save_train_config(self, *method_args, **method_kwargs):
        self.train_config = {
            'epochs': method_kwargs['epochs'],
            'learning_rate': method_kwargs['learning_rate'],
            'input_features': method_kwargs['input_features'],
            'regularization': method_kwargs['regularization'],
            'beta': method_kwargs['beta'],
            'layers_shapes': self.layers_shapes
        }
        self.logger.info('Train config: %s', self.train_config)

        return train_method(self, *method_args, **method_kwargs)

    return _save_train_config


class GCN(EmbeddingModel):
    """
    Graph Convolutional Network implementation in tensorflow.
    See https://arxiv.org/pdf/1609.02907.pdf for more
    """
    NAME = 'gcn_v1'

    def __init__(self, layers_shapes: List[int]) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.layers_shapes = layers_shapes
        self.embedding_size = layers_shapes[-1]
        self.embeddings = np.empty(shape=[0, self.embedding_size])
        self.node2idx = {}
        self.idx2node = {}

        self.tensors = {}

        self.train_config = None

    def get_name(self) -> str:
        return GCN.NAME

    def fit_args(self, graph: Graph, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        return self.fit(graph=graph,
                        epochs=epochs,
                        learning_rate=learning_rate,
                        input_features=kwargs['input_features'],
                        regularization=kwargs['regularization'],
                        beta=kwargs['beta'])

    @train_wrapper
    def fit(self,
            graph: Graph,
            learning_rate: float,
            epochs: int,
            input_features: int,
            regularization: float,
            beta: int) -> List[float]:
        tf.reset_default_graph()
        self.tensors = {}

        V = len(graph.nodes)
        self.node2idx = node2idx = create_node_idx_mapping(graph)
        self.idx2node = {v: k for k, v in node2idx.items()}

        ##################################################
        # inputs
        cac = _create_cac(graph, node2idx)
        self.tensors['cac'] = tf.constant(cac, dtype=tf.float32, shape=(V, V), name='cac', verify_shape=True)

        X = create_initial_weight(graph, node2idx, input_features, True)
        tfX = tf.constant(X, dtype=tf.float32, shape=(V, input_features), name='input_features', verify_shape=True)

        ##################################################
        # gcn
        tfH = tfX
        for i, size in enumerate(self.layers_shapes):
            tfH = self._create_gcn_layer(i, size, V, tfH)

        ##################################################
        # labels prediction
        labels_size = V

        self.tensors['target_labels'] = tf.constant(_create_target_matrix(graph, node2idx),
                                                    dtype=tf.float32,
                                                    shape=(V, labels_size),
                                                    name='target_labels',
                                                    verify_shape=False)
        self.tensors['target_weights'] = tf.Variable(
            np.random.randn(self.layers_shapes[-1], labels_size).astype(np.float32) / np.sqrt(V),
            name='target_weights', dtype=tf.float32)

        logits = tf.matmul(tfH, self.tensors['target_weights'])  # + self.tensors['target_bias']
        assert logits.shape == (V, V)

        ##################################################
        # loss
        losses = tf.nn.sigmoid_cross_entropy_with_logits(labels=self.tensors['target_labels'], logits=logits)
        B = (self.tensors['target_labels'] * (beta - 1)) + 1
        losses *= B
        loss = tf.reduce_mean(losses)
        # loss = tf.reduce_mean(tf.reduce_mean(losses, axis=1))

        loss += self._create_regularization_loss(regularization)
        tf.summary.scalar('loss', loss)

        train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss)

        ##################################################
        # accuracy
        predictions = tf.math.round(tf.nn.sigmoid(logits))

        total_true_positives = tf.reduce_sum(predictions * self.tensors['target_labels'], name='total_true_positives')
        tf.summary.scalar('total_true_positives', total_true_positives)

        total_false_positives = tf.reduce_sum(tf.clip_by_value(predictions - self.tensors['target_labels'],
                                                               clip_value_min=0, clip_value_max=1,
                                                               name='total_false_positives'))
        tf.summary.scalar('total_false_positives', total_false_positives)

        ##################################################
        # training
        costs = []
        total_time_start = time.time()
        init = tf.global_variables_initializer()
        with tf.Session() as session:
            session.run(init)

            # merge all summaries
            merged = tf.summary.merge_all()
            summary_writer = tf.summary.FileWriter(self.get_tensorboard_output_dir(), session.graph)

            self.logger.info('Starting training')

            for epoch in range(epochs):
                start_time = time.time()
                a, cost, accuracy, summary = session.run((train_op, total_true_positives, loss, merged))

                if epoch % 100 == 0:
                    costs.append(cost)
                    summary_writer.add_summary(summary, epoch)
                    self.logger.info('Epoch %d:\t%f\t(%f)', epoch, cost, time.time() - start_time)

            self.embeddings = session.run(tfH)

        self.logger.info('Training finished (%f)' % (time.time() - total_time_start))
        return costs

    def _create_gcn_layer(self, idx, size, V, tfH):
        input_size = tfH.get_shape()[1]
        weights_name = 'weights_{}'.format(idx)
        tfW = tf.Variable(initial_value=np.random.randn(input_size, size).astype(np.float32) * np.sqrt(V),
                          name=weights_name, dtype=tf.float32)
        self.tensors[weights_name] = tfW

        tmp = tfH
        tmp = tf.matmul(self.tensors['cac'], tmp)
        tmp = tf.matmul(tmp, tfW)
        tmp = tf.nn.leaky_relu(tmp, alpha=0.3)
        # tmp = tf.layers.batch_normalization(tmp)

        output_shape = tmp.get_shape()
        assert output_shape == (V, size)
        return tmp

    def _create_regularization_loss(self, regularization):
        regularization_loss = regularization * tf.nn.l2_loss(self.tensors['target_weights'])

        for i in range(len(self.layers_shapes)):
            regularization_loss += regularization * tf.nn.l2_loss(self.tensors['weights_{}'.format(i)])
        return regularization_loss

    def predict(self, node_idx: int, n: int, metric: str) -> List[int]:
        node_vec = self.embeddings[self.node2idx[node_idx]]
        distances = pairwise_distances(node_vec.reshape(1, self.embedding_size), self.embeddings, metric=metric) \
            .reshape(len(self.node2idx))
        idx = distances.argsort()[1:n]

        return list(map(lambda i: self.idx2node[i], idx))

    def get_training_params(self) -> Dict[str, object]:
        assert self.train_config is not None
        return self.train_config

    def get_node2idx(self) -> Dict[int, int]:
        return self.node2idx

    def get_embeddings(self) -> np.ndarray:
        return self.embeddings

    def save(self, dst_path: str, name_prefix: str) -> List[str]:
        embeddings_file = name_prefix + '.embeddings.npz'
        node2idx_file = name_prefix + '.node2idx.json'

        with open(os.path.join(dst_path, node2idx_file), 'w') as f:
            json.dump(self.node2idx, f)
        np.savez(os.path.join(dst_path, embeddings_file), self.embeddings)

        return [embeddings_file, node2idx_file]

    def load(self, dst_path: str, name_prefix: str) -> None:
        embeddings_file = name_prefix + '.embeddings.npz'
        node2idx_file = name_prefix + '.node2idx.json'

        with open(os.path.join(dst_path, node2idx_file)) as f:
            tmp = json.load(f)
            self.node2idx = {int(k): int(v) for k, v in tmp.items()}
            self.idx2node = {v: k for k, v in self.node2idx.items()}

        arrays = np.load(os.path.join(dst_path, embeddings_file))
        self.embeddings = arrays['arr_0']


def _create_cac(graph: Graph, node2idx: Dict[int, int]) -> np.ndarray:
    """
    Creates C_hat^-1/2   x   A_hat   x   C_hat^-1/2
    Since it doesn't change during gcn training
    """
    V = len(graph.nodes)
    a = np.zeros((V, V), dtype=np.float)
    c = np.zeros((V, V), dtype=np.float)

    for node in graph.nodes.values():
        node_idx = node2idx[node.idx]

        a[node_idx][node_idx] += 1
        c[node_idx][node_idx] = (len(node.children) + 1) ** (-1/2)

        for child in node.children:
            child_idx = node2idx[child.idx]
            a[node_idx][child_idx] += 1

    tmp = np.matmul(c, a)
    cac = np.matmul(tmp, c)
    assert cac.shape == (V, V)

    return cac


def _create_target_matrix(graph: Graph, node2idx: Dict[int, int]) -> np.ndarray:
    """
    Create target matrix - parent categories adjacency
    """
    V = len(graph.nodes)
    labels = np.zeros((V, V), dtype=np.float)

    for node in graph.nodes.values():
        node_idx = node2idx[node.idx]

        for parent in node.parents:
            parent_idx = node2idx[parent.idx]
            labels[node_idx][parent_idx] = 1

    return labels
