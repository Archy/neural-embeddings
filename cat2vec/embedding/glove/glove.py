import json
import logging
import os
import time
from enum import Enum
from functools import wraps
from typing import List, Dict, Tuple

import numpy as np
import tensorflow as tf
from sklearn.metrics.pairwise import pairwise_distances

from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.embedding.glove.coocurence_matrix import cc_matrix_from_random_walks, cc_matrix_from_articles_assignment
from cat2vec.graph.graph import Graph
from cat2vec.utils.common import create_node_idx_mapping


def train_wrapper(train_method):
    """
    Decorator that saves training config to dictionary
    """

    @wraps(train_method)
    def _save_train_config(self, *method_args, **method_kwargs):
        self.train_config = {
            'epochs': method_kwargs['epochs'],
            'learning_rate': method_kwargs['learning_rate'],
            'reg': method_kwargs['reg'],
        }

        cc_type, config = validate_and_get_cc_matrix_config(**method_kwargs)
        if cc_type == CoocurrenceMatrixType.FROM_RANDOM_WALKS:
            cc_config = {
                'window_size': config[1],
            }
        else:
            cc_config = {
                'max_level': config
            }
        self.train_config['cc_type'] = cc_type
        self.train_config['cc_config'] = cc_config

        self.logger.debug('Train config: %s', self.train_config)
        return train_method(self, *method_args, **method_kwargs)

    return _save_train_config


class Glove(EmbeddingModel):
    """
    Glove implementation in tensorflow.
    See https://nlp.stanford.edu/pubs/glove.pdf for more.
    """
    NAME = 'glove_v1'
    WEIGHTS_NAME = NAME + '.weights.npz'
    NODE2IDX_NAME = NAME + '.node2idx.json'

    def __init__(self, embedding_size) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.embedding_size = embedding_size
        self.node2idx = {}
        self.idx2node = {}
        self.W = np.empty(shape=[0, embedding_size])
        self.U = np.empty(shape=[0, embedding_size])
        self.embeddings = np.empty(shape=[0, embedding_size])
        self.train_config = None
        self.cc_matrix_memento = None

    def get_name(self) -> str:
        return Glove.NAME

    def fit_args(self, graph: Graph, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        return self.fit(
            graph=graph,
            epochs=epochs,
            learning_rate=learning_rate,
            **kwargs
        )

    @train_wrapper
    def fit(self,
            graph: Graph,
            epochs: int,
            learning_rate: float,
            reg: float = 0.1,
            **kwargs
            ) -> List[float]:
        tf.reset_default_graph()

        V = len(graph.nodes)
        D = self.embedding_size
        self.node2idx = node2idx = create_node_idx_mapping(graph)
        self.idx2node = {v: k for k, v in node2idx.items()}

        # co-occurrence matrix
        if self.cc_matrix_memento is None:
            self.cc_matrix_memento = CoocurenceMatrixMemento(graph, node2idx)
        weighted_cc, target_cc = self.cc_matrix_memento.get_cc_matrix(**kwargs)
        assert weighted_cc is not None
        assert target_cc is not None

        # input
        tf_weighted_cc = tf.placeholder(tf.float32, shape=(V, V), name='weighted_cc_matrix')
        tf_target_cc = tf.placeholder(tf.float32, shape=(V, V), name='target_cc_matrix')

        # weights
        W = np.random.randn(V, D) / np.sqrt(V + D)
        tfW = tf.Variable(W, dtype=tf.float32)

        b = np.zeros(V).reshape(V, 1)
        tfb = tf.Variable(b, dtype=tf.float32)

        U = np.random.randn(V, D) / np.sqrt(V + D)
        tfU = tf.Variable(U, dtype=tf.float32)

        mu = target_cc.mean()

        # model
        tmp = tf.matmul(tfW, tf.transpose(tfU)) + tfb + mu - tf_target_cc
        loss = tf.reduce_sum(tf_weighted_cc * tmp * tmp)
        for param in (tfW, tfU):
            loss += reg * tf.reduce_sum(param * param)
        tf.summary.scalar('loss', loss)

        train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss)

        init = tf.global_variables_initializer()
        costs = []
        total_time_start = time.time()
        with tf.Session() as session:
            session.run(init)
            start_time = time.time()

            # merge all summaries
            merged = tf.summary.merge_all()
            summary_writer = tf.summary.FileWriter(self.get_tensorboard_output_dir(), session.graph)

            self.logger.info('Starting training')

            for epoch in range(epochs):
                _, cost, summary = session.run(
                    (train_op, loss, merged),
                    feed_dict={
                        tf_weighted_cc: weighted_cc,
                        tf_target_cc: target_cc
                    }
                )
                costs.append(cost)
                summary_writer.add_summary(summary, epoch)
                self.logger.info('Epoch %d:\t%f\t(%f)', epoch, cost, time.time() - start_time)

            self.W, self.U = session.run([tfW, tfU])
            self.embeddings = (self.W + self.U) / 2

        self.logger.info('Training finished (%f)' % (time.time() - total_time_start))
        return costs

    def predict(self, node_idx: int, n: int, metric: str) -> List[int]:
        node_vec = self.embeddings[self.node2idx[node_idx]]
        distances = pairwise_distances(node_vec.reshape(1, self.embedding_size), self.embeddings, metric=metric) \
            .reshape(len(self.node2idx))
        idx = distances.argsort()[1:n]

        return list(map(lambda i: self.idx2node[i], idx))

    def get_training_params(self) -> Dict[str, object]:
        assert self.train_config is not None
        return self.train_config

    def get_node2idx(self) -> Dict[int, int]:
        return self.node2idx

    def get_embeddings(self) -> np.ndarray:
        return self.embeddings

    def save(self, dst_path: str, name_prefix: str) -> List[str]:
        weights_file = name_prefix + Glove.WEIGHTS_NAME
        node2idx_file = name_prefix + Glove.NODE2IDX_NAME

        with open(os.path.join(dst_path, node2idx_file), 'w') as f:
            json.dump(self.node2idx, f)
        arrays = [self.W, self.U]
        np.savez(os.path.join(dst_path, weights_file), *arrays)

        return [weights_file, node2idx_file]

    def load(self, dst_path: str, name_prefix: str) -> None:
        self.logger.info('Loading model')
        weights_file = name_prefix + Glove.WEIGHTS_NAME
        node2idx_file = name_prefix + Glove.NODE2IDX_NAME

        with open(os.path.join(dst_path, node2idx_file)) as f:
            tmp = json.load(f)
            self.node2idx = {int(k): int(v) for k, v in tmp.items()}
            self.idx2node = {v: k for k, v in self.node2idx.items()}
            self.logger.info('Loaded node2idx')

        arrays = np.load(os.path.join(dst_path, weights_file))
        self.W = arrays['arr_0']
        self.U = arrays['arr_1']
        self.embeddings = (self.W + self.U) / 2
        self.logger.info('Loaded weights')


def validate_and_get_cc_matrix_config(**kwargs):
    # from random walks
    walks = kwargs['walks'] if 'walks' in kwargs else None
    window_size = kwargs['window_size'] if 'window_size' in kwargs else None

    # from articles assignment
    max_level = kwargs['max_level'] if 'max_level' in kwargs else None

    if max_level is not None:
        assert walks is None
        assert window_size is None
        return CoocurrenceMatrixType.FROM_ARTICLES_ASSIGNMENT, max_level
    else:
        assert walks is not None
        assert window_size is not None
        return CoocurrenceMatrixType.FROM_RANDOM_WALKS, (walks, window_size)


class CoocurrenceMatrixType(Enum):
    FROM_RANDOM_WALKS = 1
    FROM_ARTICLES_ASSIGNMENT = 2


class CoocurenceMatrixMemento:
    def __init__(self, graph: Graph, node2idx: Dict[int, int]) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)
        self.graph = graph
        self.node2idx = node2idx

        self.cc_matrices = None
        self.prev_type = None
        self.prev_config = None

    def get_cc_matrix(self, **kwargs) -> Tuple[np.ndarray, np.ndarray]:
        """
        Creates coocurence matrix type based on configuration passed in kwargs
        """
        cc_type, config = validate_and_get_cc_matrix_config(**kwargs)

        if self.cc_matrices is None or \
                (self.prev_type != cc_type) or \
                (self.prev_config != config):
            self.cc_matrices = self._create_cc_matrix(cc_type, config)
            self.prev_config = config
            self.prev_type = cc_type
        else:
            self.logger.info('Reusing existing coocurence matrix')

        return self.cc_matrices

    def _create_cc_matrix(self, cc_type, config) -> Tuple[np.ndarray, np.ndarray]:

        if cc_type == CoocurrenceMatrixType.FROM_RANDOM_WALKS:
            self.logger.info('Creating coocurence matrix from random walks')
            weighted_cc, target_cc = cc_matrix_from_random_walks(
                graph=self.graph,
                node2idx=self.node2idx,
                walks=config[0],
                window_size=config[1],
            )
        else:
            self.logger.info('Creating coocurence matrix from articles assignment')
            weighted_cc, target_cc = cc_matrix_from_articles_assignment(
                graph=self.graph,
                node2idx=self.node2idx,
                max_level=config
            )
        return weighted_cc, target_cc