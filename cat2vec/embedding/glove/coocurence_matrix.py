import logging
import time
from typing import List, Dict, Tuple
from cat2vec.graph.graph import Graph, Node
import numpy as np


logger = logging.getLogger(__name__)


def cc_matrix_from_articles_assignment(graph: Graph,
                                       node2idx: Dict[int, int],
                                       max_level=None,
                                       cc_max: int = 45,
                                       alpha: float = 0.75) -> Tuple[np.ndarray, np.ndarray]:
    """
    Creates co-occurrence matrix based on articles assignment to categories.

    :param graph: a graph
    :param node2idx: dict mapping nodes idx to consecutive indices in an array
    :param max_level: max level of recursion for subcategories articles assignment
    :param cc_max: max co-occurrence count to clip value at
    :param alpha: weighting function factor
    :return: tuple of input and target matrix
    """
    V = len(graph.nodes)
    cc_matrix = np.zeros((V, V))

    total_nodes = len(graph.nodes)
    processed = 0

    total_time_start = time.time()
    for idx, node in graph.nodes.items():
        for ido, other in graph.nodes.items():
            if idx == ido:
                continue
            i_node = node2idx[idx]
            i_other = node2idx[ido]

            for art in node.articles:
                if art in other.articles:
                    cc_matrix[i_node, i_other] += 1

            _rec_coocurrence(cc_matrix, i_node, i_other, node, other, 2, max_level)
        processed += 1
        if processed % 100 == 0:
            logger.info('%d / %d' % (processed, total_nodes))
    logger.info('Creating cc matrix finished (%f)' % (time.time() - total_time_start))

    return _create_weighted_cc_matrix(V, cc_matrix, cc_max, alpha)


def _rec_coocurrence(matrix: np.ndarray, i_node: int, i_other: int, node: Node, other: Node, level: int, max_level):
    if max_level is not None and level > max_level:
        return

    for c in node.children:
        for art in c.articles:
            if art in other.articles:
                matrix[i_node, i_other] += 1 / level
        _rec_coocurrence(matrix, i_node, i_other, c, other, level + 1, max_level)


def cc_matrix_from_random_walks(graph: Graph,
                                node2idx: Dict[int, int],
                                walks: List[List[int]],
                                window_size: int = 5,
                                cc_max: int = 45,
                                alpha: float = 0.75) -> Tuple[np.ndarray, np.ndarray]:
    """
    Creates co-occurrence matrix based on given random walks.

    :param graph: a graph
    :param node2idx: dict mapping nodes idx to consecutive indices in an array
    :param walks: random walks
    :param window_size: context size for each node in a random walk
    :param cc_max: max co-occurrence count to clip value at
    :param alpha: weighting function factor
    :return: tuple of input and target matrix
    """
    V = len(graph.nodes)
    cc_matrix = np.zeros((V, V))

    total_time_start = time.time()
    for walk in walks:
        if len(walk) < 2:
            continue

        for i, node in enumerate(walk):
            if i < 1:
                continue
            node_idx = node2idx[node]
            for j in range(max(0, i - window_size), i):
                c_idx = node2idx[walk[j]]
                cc_matrix[c_idx, node_idx] += np.abs(1 / (i - j))
            # this is not actually correct:
            for j in range(i + 1, min(len(walk), i + window_size)):
                c_idx = node2idx[walk[j]]
                cc_matrix[c_idx, node_idx] += np.abs(0.1 / (j - i))
    logger.info('Creating cc matrix finished (%f)' % (time.time() - total_time_start))

    return _create_weighted_cc_matrix(V, cc_matrix, cc_max, alpha)


def _create_weighted_cc_matrix(V: int, cc: np.ndarray,
                               cc_max: int, alpha: float) -> Tuple[np.ndarray, np.ndarray]:
    """
    Creates weighted co-occurrence matrix and target matrix

    :param V: number of nodes in graph
    :param cc: co-occurrence matrix
    :param cc_max: max co-occurrence count to clip value at
    :param alpha: weighting function factor
    :return: tuple of input and target matrix
    """
    weighted_cc = np.zeros((V, V))
    weighted_cc[cc < cc_max] = (cc[cc < cc_max] / float(cc_max)) ** alpha
    weighted_cc[cc >= cc_max] = 1

    return weighted_cc, np.log(cc + 1)
