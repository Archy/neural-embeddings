import logging
import time
from collections import defaultdict
from typing import Generator, Dict

import math
import numpy as np

from cat2vec.graph.graph import Graph


class SdneGenerator:
    """
    Generator for :class:`.sdne.SDNE`.
    Train generator returns tuple of:
        - array of global adjacency vector (batch_size x V)
        - batch adjacency matrix (batch_size x batch_size)
    """

    def __init__(self, graph: Graph, node2idx: Dict[int, int], batch_size: int) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.graph = graph
        self.node2idx = node2idx
        self.batch_size = batch_size

        # count steps_per_epoch
        total_samples = 0
        for node in graph.nodes.values():
            total_samples += 1
            total_samples += len(node.children)
        self.steps_per_epoch = math.floor(total_samples / batch_size)
        self.logger.info('total samples: {}\tbatch size: {}\t steps per epoch: {}'.format(total_samples, batch_size,
                                                                                          self.steps_per_epoch))

    def generator(self) -> Generator:
        """
        :return: the actual generator
        """
        nodes = list(self.graph.nodes.values())
        while True:

            batches_count = 0
            batch_time = time.time()
            total_time = 0

            batch_size = self.batch_size
            V = len(self.graph.nodes)
            target = np.empty((1, 1, 1), dtype=np.float32)

            batch_nodes = set()
            batch2idx = defaultdict(lambda: [])
            batch_vectors = np.zeros((batch_size, V))
            curr_index = 0

            np.random.shuffle(nodes)
            for node in nodes:
                for case in [node, *node.children]:
                    batch_nodes.add(case)
                    batch2idx[case.idx].append(curr_index)
                    for child in case.children:
                        batch_vectors[curr_index][self.node2idx[child.idx]] = 1
                    for parent in case.parents:
                        batch_vectors[curr_index][self.node2idx[parent.idx]] = 1

                    curr_index += 1
                    if curr_index == batch_size:
                        self.logger.debug('Batch creation time: %f', (time.time() - batch_time))
                        batch_vectors = batch_vectors[np.newaxis, :]
                        yield [batch_vectors, self._create_batch_adjacency(batch_nodes, batch2idx)[np.newaxis, :]], \
                              [target, batch_vectors]
                        batches_count += 1
                        if batches_count == self.steps_per_epoch:
                            break

                        batch_nodes = set()
                        batch2idx = defaultdict(lambda: [])
                        batch_vectors = np.zeros((batch_size, V))
                        curr_index = 0
                        batch_time = time.time()
                if batches_count == self.steps_per_epoch:
                    break

            self.logger.info('Batches generated: %d in %f', batches_count, total_time)

    def _create_batch_adjacency(self, batch_nodes, batch2idx):
        anything_set = False
        batch_adjacency = np.zeros((self.batch_size, self.batch_size))
        for node in batch_nodes:
            for child in node.children:
                if child in batch_nodes:
                    for child_batch_id in batch2idx[child.idx]:
                        batch_adjacency[batch2idx[node.idx], child_batch_id] = 1
                        anything_set = True
            for parent in node.parents:
                if parent in batch_nodes:
                    for parent_batch_id in batch2idx[parent.idx]:
                        batch_adjacency[batch2idx[node.idx], parent_batch_id] = 1
                        anything_set = True

        if not anything_set:
            self.logger.debug('Batch adjacency is empty')
        return batch_adjacency

    def embedding_generator(self):
        """
        Return generator, that produces sorted (as in node2idx) adjacency vectors for nodes
        :return:
        """
        V = len(self.graph.nodes)
        idx2node = {v: k for k, v in self.node2idx.items()}

        for i in range(V):
            vector = np.zeros((V,))
            node = self.graph.nodes[idx2node[i]]
            for child in node.children:
                vector[self.node2idx[child.idx]] = 1
            for parent in node.parents:
                vector[self.node2idx[parent.idx]] = 1
            yield [vector]
