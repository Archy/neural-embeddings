import json
import logging
import os
from functools import wraps
from typing import List, Dict, Optional

import numpy as np
import tensorflow as tf
from sklearn.metrics import pairwise_distances

from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.graph.graph import Graph
from cat2vec.embedding.sdne.sdne_generator import SdneGenerator
from cat2vec.utils.common import create_node_idx_mapping


def _l1_loss(_, y_pred, input_adj):
    # get rid of batch axis, since keras batch is always 1
    y_pred = y_pred[0]
    input_adj = input_adj[0]

    D = tf.diag(tf.reduce_sum(input_adj, 1))
    L = D - input_adj
    loss = 2 * tf.trace(tf.matmul(tf.matmul(tf.transpose(y_pred), L), y_pred))
    return tf.expand_dims(loss, axis=0)


def _l2_loss(y_true, y_pred, beta):
    # get rid of batch axis, since keras batch is always 1.
    y_true = y_true[0]
    y_pred = y_pred[0]

    B = (y_true * (beta - 1)) + 1
    per_node_loss = tf.math.reduce_sum(tf.math.square((y_pred - y_true) * B), axis=1)
    loss = tf.math.reduce_mean(per_node_loss)
    return tf.expand_dims(loss, axis=0)


def training_accuracy(y_true, y_pred):
    """
    Mean 'true positive' adjacency prediction rate per each node
    """
    # count accuracy only from output of regular softmax
    y_true = y_true[0]
    y_pred = y_pred[0]

    tf_true_positives = tf.cast(tf.equal(y_true, tf.math.round(y_pred)), tf.float32) * y_true
    tf_correct_true_predictions = tf.math.reduce_sum(tf_true_positives, axis=1)

    tf_total_edges = tf.math.reduce_sum(y_true, axis=1)

    tf_per_node_accuracy = tf_correct_true_predictions / tf_total_edges
    tf_mean_accuracy = tf.reduce_mean(tf_per_node_accuracy)

    return tf.expand_dims(tf_mean_accuracy, axis=0)


def train_wrapper(train_method):
    """
    Decorator that saves training config to dictionary
    """

    @wraps(train_method)
    def _save_train_config(self, *method_args, **method_kwargs):
        self.train_config = {
            'epochs': method_kwargs['epochs'],
            'learning_rate': method_kwargs['learning_rate'],
            'batch_size': method_kwargs['batch_size'],
            'regularization': method_kwargs['regularization'],
            'beta': method_kwargs['beta'],
            'loss1_weight': method_kwargs['loss1_weight'],
            'loss2_weight': method_kwargs['loss2_weight'],
            'additional_layers': self.additional_hidden_layers_shapes,
            'embeddings size': self.embedding_size,
        }
        self.logger.info('Train config: %s', self.train_config)

        return train_method(self, *method_args, **method_kwargs)

    return _save_train_config


class SDNE(EmbeddingModel):
    """
    Structural Deep Network Embedding (SDNE) implementation in tensorflow/keras.
    See https://www.kdd.org/kdd2016/papers/files/rfp0191-wangAemb.pdf for more.

    The autoencoder has following architecture:
        (decoder output)
        |V|
        <reverted optional additional hidden layers>
        (decoder input)

        (encoder output)
        embedding_size
        <optional additional hidden layers>
        |V|
        (encoder input)

    Note on the batch size: there are actually 2 batch sizes here. One is 'keras batch size' which is always 1, and
    the other is 'data generator batch size' (the actual batch size). We need to know all nodes in a batch to create
    batch adjacency matrix and i don't see better way to workaround it right now.
    Fixme: remove 'keras batch size' or somehow combine it wit the 'data generator batch size'
    """
    NAME = 'sdne_v1'

    def __init__(self, embedding_size: int,
                 additional_hidden_layers_shapes: Optional[List[int]] = None,
                 use_leaky_relu: bool = True) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.embedding_size = embedding_size
        self.embeddings = np.empty(shape=[0, embedding_size])

        self.node2idx = {}
        self.idx2node = {}

        self.additional_hidden_layers_shapes = \
            additional_hidden_layers_shapes if additional_hidden_layers_shapes is not None else []
        self.model: tf.keras.models.Model = None
        self.encoder: tf.keras.models.Model = None

        self.use_leaky_relu = use_leaky_relu
        self.train_config = None

    def get_name(self) -> str:
        return SDNE.NAME

    def fit_args(self, graph: Graph, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        return self.fit(graph=graph,
                        epochs=epochs,
                        learning_rate=learning_rate,
                        batch_size=kwargs['batch_size'],
                        regularization=kwargs['regularization'],
                        loss1_weight=kwargs['loss1_weight'],
                        loss2_weight=kwargs['loss2_weight'],
                        beta=kwargs['beta'])

    @train_wrapper
    def fit(self,
            graph: Graph,
            epochs: int,
            learning_rate: float,
            beta: float,
            batch_size: int,
            regularization: float,
            loss1_weight: float,
            loss2_weight: float):
        # clear everything just in case
        tf.keras.backend.clear_session()
        tf.reset_default_graph()

        V = len(graph.nodes)
        self.node2idx = node2idx = create_node_idx_mapping(graph)
        self.idx2node = {v: k for k, v in node2idx.items()}

        model, encoder, layers = self._create_model(V,
                                                    learning_rate,
                                                    regularization,
                                                    beta,
                                                    loss1_weight,
                                                    loss2_weight)
        # prepare data generator
        sdne_generator = SdneGenerator(
            graph=graph,
            node2idx=self.node2idx,
            batch_size=batch_size
        )

        # fit model
        self.logger.info('training model')
        callbacks = [
            tf.keras.callbacks.TensorBoard(log_dir=self.get_tensorboard_output_dir(), histogram_freq=0,
                                           batch_size=1, write_graph=True, update_freq='epoch'),
        ]
        history = model.fit_generator(
            generator=sdne_generator.generator(),
            steps_per_epoch=sdne_generator.steps_per_epoch,
            epochs=epochs,
            verbose=2,
            callbacks=callbacks,
            max_queue_size=100,
        )

        # create embeddings
        self._create_embeddings(encoder, sdne_generator)

        self.model = model
        self.encoder = encoder

        return history.history['loss']

    def _create_model(self,
                      V: int,
                      learning_rate: float,
                      regularization: float,
                      beta: float,
                      loss1_weight: float,
                      loss2_weight: float):
        assert beta > 1

        layers = {
            'input_x': tf.keras.layers.Input(shape=(None, V), name='input_x', dtype=tf.float32, batch_size=1),
            'input_adj': tf.keras.layers.Input(shape=(None, None), name='input_adj', dtype=tf.float32, batch_size=1),
        }

        def create_layers(name, shapes, the_input):
            x = the_input
            for i, shape in enumerate(shapes):
                if self.use_leaky_relu:
                    layer_name = '{}_{}-units'.format(name, i)
                    x = tf.keras.layers.Dense(name=layer_name,
                                              units=shape,
                                              activation='linear',
                                              kernel_regularizer=tf.keras.regularizers.l2(regularization),
                                              use_bias=True)(x)
                    layers[layer_name] = x

                    layer_name = '{}_{}'.format(name, i)
                    x = tf.keras.layers.LeakyReLU(name=layer_name)(x)
                    layers[layer_name] = x
                else:
                    layer_name = '{}_{}'.format(name, i)
                    x = tf.keras.layers.Dense(name=layer_name,
                                              units=shape,
                                              activation='sigmoid',
                                              kernel_regularizer=tf.keras.regularizers.l2(regularization),
                                              use_bias=True)(x)
                    layers[layer_name] = x
            return x

        layers_shapes = self.additional_hidden_layers_shapes
        encoder_output = create_layers('encoder', [V, *layers_shapes, self.embedding_size], layers['input_x'])
        assert encoder_output.shape.as_list() == [1, None, self.embedding_size]

        tmp = create_layers('decoder', layers_shapes[::-1], encoder_output)
        decoder_output_name = 'decoder_{}'.format(len(layers_shapes))
        decoder_output = tf.keras.layers.Dense(name=decoder_output_name,
                                               units=V,
                                               activation='sigmoid',
                                               use_bias=True,
                                               kernel_regularizer=tf.keras.regularizers.l2(regularization))(tmp)
        layers[decoder_output_name] = decoder_output

        assert decoder_output.shape.as_list() == [1, None, V]

        # full model

        optimizer = tf.keras.optimizers.Adam(lr=learning_rate)
        model = tf.keras.models.Model(
            inputs=[layers['input_x'], layers['input_adj']],
            outputs=[encoder_output, decoder_output]
        )
        model.compile(
            optimizer=optimizer,
            loss=[
                lambda y_true, y_pred: _l1_loss(y_true, y_pred, layers['input_adj']),
                lambda y_true, y_pred: _l2_loss(y_true, y_pred, beta),
            ],
            loss_weights=[
                loss1_weight,
                loss2_weight
            ],
            metrics={
                '{}_{}'.format('decoder', len(layers_shapes)): training_accuracy
            }
        )
        self.logger.info(model.summary())

        # encoder

        encoder = tf.keras.Model(
            inputs=[layers['input_x']],
            outputs=[layers['{}_{}'.format('encoder', len(layers_shapes) + 1)]]
        )

        return model, encoder, layers

    def _create_embeddings(self, encoder: tf.keras.Model, sdne_generator: SdneGenerator):
        self.logger.info('Creating embeddings')

        tmp_embeddings = []
        for i, vector in enumerate(sdne_generator.embedding_generator()):
            embedding = encoder.predict(np.asarray(vector)[np.newaxis, :])
            tmp_embeddings.append(embedding[0][0])
        self.embeddings = np.asarray(tmp_embeddings)

        self.logger.info('Embeddings created, shape: ' + str(self.embeddings.shape))

    def predict(self, node_idx: int, n: int, metric: str) -> List[int]:
        node_vec = self.embeddings[self.node2idx[node_idx]]
        distances = pairwise_distances(node_vec.reshape(1, self.embedding_size), self.embeddings, metric=metric) \
            .reshape(len(self.node2idx))
        idx = distances.argsort()[1:n]

        return list(map(lambda i: self.idx2node[i], idx))

    def get_training_params(self) -> Dict[str, object]:
        assert self.train_config is not None
        return self.train_config

    def get_node2idx(self) -> Dict[int, int]:
        return self.node2idx

    def get_embeddings(self) -> np.ndarray:
        return self.embeddings

    def save(self, dst_path: str, name_prefix: str) -> List[str]:
        model_file = name_prefix + '.model.h5'
        encoder_file = name_prefix + '.encoder.h5'
        embeddings_file = name_prefix + '.embeddings.npz'
        node2idx_file = name_prefix + '.node2idx.json'

        self.model.save(os.path.join(dst_path, model_file))
        self.encoder.save(os.path.join(dst_path, encoder_file))
        np.savez(os.path.join(dst_path, embeddings_file), self.embeddings)
        with open(os.path.join(dst_path, node2idx_file), 'w') as f:
            json.dump(self.node2idx, f)

        return [model_file, encoder_file, embeddings_file, node2idx_file]

    def load(self, dst_path: str, name_prefix: str) -> None:
        model_file = name_prefix + '.model.h5'
        encoder_file = name_prefix + '.encoder.h5'
        embeddings_file = name_prefix + '.embeddings.npz'
        node2idx_file = name_prefix + '.node2idx.json'

        self.model = tf.keras.models.load_model(os.path.join(dst_path, model_file),
                                                custom_objects={'<lambda>': lambda y_true, y_pred: y_pred,
                                                                'training_accuracy': training_accuracy})
        self.encoder = tf.keras.models.load_model(os.path.join(dst_path, encoder_file))

        with open(os.path.join(dst_path, node2idx_file)) as f:
            tmp = json.load(f)
            self.node2idx = {int(k): int(v) for k, v in tmp.items()}
            self.idx2node = {v: k for k, v in self.node2idx.items()}

        arrays = np.load(os.path.join(dst_path, embeddings_file))
        self.embeddings = arrays['arr_0']
