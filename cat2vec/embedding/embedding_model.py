import os
import time

import numpy as np
from abc import ABCMeta, abstractmethod
from typing import Dict, List

from cat2vec.graph.graph import Graph


class EmbeddingModel:
    """
    Interface required for model passed to EmbeddingsAssessor
    """
    __metaclass__ = ABCMeta
    TENSORBOARD_OUTPUT_PATH = r'C:\Users\Jan\Desktop\neural-embeddings\tensorboard'

    @abstractmethod
    def get_name(self) -> str:
        """
        :return: name of the model
        """
        raise NotImplementedError

    @abstractmethod
    def fit_args(self, graph: Graph, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        """
        Trains model on given data. Common call to all models

        :param graph: the graph
        :param learning_rate: learning rate
        :param epochs: number of training epochs
        :param kwargs: additional training parameters, specific to model.
        :return: list of losses for each epoch
        """
        raise NotImplementedError

    @abstractmethod
    def predict(self, node_idx: int, n: int, metric: str) -> List[int]:
        """

        :param node_idx:
        :param n:
        :param metric:
        :return:
        """
        raise NotImplementedError

    @abstractmethod
    def get_training_params(self) -> Dict[str, object]:
        """
        :return: configuration used during training
        """
        raise NotImplementedError

    @abstractmethod
    def get_node2idx(self) -> Dict[int, int]:
        """
        :return: dict mapping nodes idx to consecutive indices in an W array
        """
        raise NotImplementedError

    @abstractmethod
    def get_embeddings(self) -> np.ndarray:
        """
        :return: nodes features vectors as numpy matrix
        """
        raise NotImplementedError

    @abstractmethod
    def save(self, dst_path: str, name_prefix: str) -> List[str]:
        """
        Saves the model
        :param dst_path: destination directory path
        :param name_prefix: prefix to use for saving files
        :return: list of saved files
        """
        raise NotImplementedError

    @abstractmethod
    def load(self, dst_path: str, name_prefix: str) -> None:
        """
        Load model from given directory
        :param dst_path:
        :param name_prefix:
        """
        raise NotImplementedError

    def get_tensorboard_output_dir(self):
        timestamp = time.time()
        run_directory = '{}-{}'.format(self.get_name(), timestamp)
        return os.path.join(EmbeddingModel.TENSORBOARD_OUTPUT_PATH, run_directory)
