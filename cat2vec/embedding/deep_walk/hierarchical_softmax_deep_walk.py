import json
import logging
import os
import time
from functools import wraps
from typing import List, Dict

import numpy as np
import tensorflow as tf
from sklearn.metrics.pairwise import pairwise_distances

from cat2vec.embedding.deep_walk.hierarchical_softmax_generator import HierarchicalGenerator
from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.graph.graph import Graph
from cat2vec.utils.common import create_node_idx_mapping, create_initial_weight


def train_wrapper(train_method):
    """
    Decorator that saves training config to dictionary
    """
    @wraps(train_method)
    def _save_train_config(self, *method_args, **method_kwargs):
        self.train_config = {
            'epochs': method_kwargs['epochs'],
            'batch_size': method_kwargs['batch_size'],
            'learning_rate': method_kwargs['learning_rate'],
            'window_size': method_kwargs['window_size'],
            'use_glove_init': method_kwargs['use_glove_init'],
            'type': 'hierarchical_sotfmax',
            'embeddings_size': self.embedding_size,
        }
        self.logger.info('Train config: %s', self.train_config)

        return train_method(self, *method_args, **method_kwargs)
    return _save_train_config


class DeepWalk(EmbeddingModel):
    """
    DeepWalk (hierarchical softmax based) implementation in tensorflow.
    See http://www.perozzi.net/publications/14_kdd_deepwalk.pdf for more.
    """
    EINSUM_EQUATION = 'ijk,ilk->ij'
    NAME = 'deepWalk_hierarchicalSoftmax_v1'
    WEIGHTS_NAME = '.weights.npz'
    NODE2IDX_NAME = '.node2idx.json'

    def __init__(self, embedding_size):
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.embedding_size = embedding_size
        self.W = np.empty(shape=[0, embedding_size])
        self.node2idx = {}
        self.idx2node = {}
        self.train_config = None

    def get_name(self) -> str:
        return DeepWalk.NAME

    def fit_args(self, graph: Graph, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        return self.fit(
            graph=graph,
            walks=kwargs['walks'],
            epochs=epochs,
            batch_size=kwargs['batch_size'],
            learning_rate=learning_rate,
            window_size=kwargs['window_size'],
            use_glove_init=kwargs['use_glove_init'] if 'use_glove_init' in kwargs else False
        )

    @train_wrapper
    def fit(self, *, graph: Graph,
            walks: List[List[int]],
            epochs: int = 100,
            batch_size: int = 10,
            learning_rate: float = 0.01,
            window_size: int = 5,
            use_glove_init: bool = False,
            ) -> List[float]:
        tf.reset_default_graph()

        V = len(graph.nodes)
        D = self.embedding_size
        self.node2idx = node2idx = create_node_idx_mapping(graph)
        self.idx2node = {v: k for k, v in node2idx.items()}

        # input generator
        samples_generator = HierarchicalGenerator(
            graph=graph,
            node2idx=node2idx,
            walks=walks,
            window_size=window_size,
            batch_size=batch_size
        )

        dataset = tf.data.Dataset.from_generator(samples_generator.generator,
                                                 output_types=(tf.int32, tf.int32, tf.float32, tf.int32, tf.float32),
                                                 output_shapes=((batch_size, 1),
                                                                (batch_size, None), (batch_size, None, 1),
                                                                (batch_size, None), (batch_size, None, 1)))
        train_iterator = dataset.repeat(1).prefetch(100).make_initializable_iterator('train_iterator')
        tf_input, tf_left_indices, tf_left_mask, tf_right_indices, tf_right_mask = train_iterator.get_next()

        # input weights:
        W = create_initial_weight(graph, node2idx, D, use_glove_init)
        tfW = tf.Variable(W, dtype=tf.float32)
        tf.summary.histogram("input_weights", tfW)

        # softmax weights
        V = np.random.randn(V - 1, D).astype(np.float32) / np.sqrt(V-1)
        tfV = tf.Variable(V, dtype=tf.float32)
        tf.summary.histogram("softmax_weights", tfV)

        # model
        embedded_input = tf.nn.embedding_lookup(tfW, tf_input)

        # 'left children'
        # change nodes ids to values from softmax tree
        pos_v = tf.gather(tfV, tf_left_indices)
        self.logger.debug('pos_v %s', pos_v.shape)
        # multiply by mask to handle padding in a batch:
        pos_v_masked = tf.multiply(pos_v, tf_left_mask)
        self.logger.debug('pos_v_masked %s', pos_v_masked.shape)
        # multiply by input embedding and reduce-multiply along each batch
        self.logger.debug('embedded_input: %s', embedded_input.shape)
        pos_einsum = tf.einsum(DeepWalk.EINSUM_EQUATION, pos_v_masked, embedded_input)  # ~dot product along 3rd axis
        self.logger.debug('einsum: %s', pos_einsum.shape)
        # sigmoid - left/right probability
        pos_probability = tf.sigmoid(pos_einsum)
        self.logger.debug('pos_probablity: %s', pos_probability)
        pos_logits = tf.math.reduce_prod(pos_probability, axis=1, keepdims=True)
        self.logger.debug('pos_logits: %s', pos_logits.shape)

        # 'right children'
        neg_v = tf.gather(tfV, tf_right_indices)
        neg_v_masked = tf.multiply(neg_v, tf_right_mask)
        neg_einsum = tf.einsum(DeepWalk.EINSUM_EQUATION, neg_v_masked, embedded_input)
        neg_probability = tf.sigmoid(-neg_einsum)
        # tf_one = tf.constant(1, dtype=tf.float32)
        # neg_inv = tf.math.subtract(tf_one, neg_probability)  # P(going right) = 1 - P(going left)
        neg_logits = tf.math.reduce_prod(neg_probability, axis=1, keepdims=True)

        # loss = tf.math.reduce_mean(-tf.log(tf.clip_by_value(pos_logits * neg_logits, 1e-10, 1.0)))
        # loss = tf.math.reduce_mean(-tf.log(pos_logits * neg_logits))

        tf_one = tf.constant(1, dtype=tf.float32, shape=(batch_size, 1))
        loss = tf.losses.log_loss(labels=tf_one, predictions=tf.multiply(pos_logits, neg_logits))

        tf.summary.scalar('loss', loss)
        train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss)

        init = tf.global_variables_initializer()
        costs = []
        total_time_start = time.time()
        with tf.Session() as session:
            session.run(init)

            # merge all summaries
            merged = tf.summary.merge_all()
            summary_writer = tf.summary.FileWriter(self.get_tensorboard_output_dir(), session.graph)

            self.logger.info('Starting training')

            for epoch in range(epochs):
                session.run(train_iterator.initializer)
                start_time = time.time()

                cost = 0
                try:
                    while True:
                        _, c, summary = session.run(
                            (train_op, loss, merged)
                        )
                        cost += c
                except tf.errors.OutOfRangeError:
                    pass

                costs.append(cost)
                summary_writer.add_summary(summary, epoch)
                self.logger.info('Epoch %d:\t%f\t(%f)', epoch, cost, time.time() - start_time)

            self.W = session.run(tfW)

        self.logger.info('Training finished (%f)' % (time.time() - total_time_start))
        return costs

    def predict(self, node_idx: int, n: int, metric: str) -> List[int]:
        node_vec = self.W[self.node2idx[node_idx]]
        distances = pairwise_distances(node_vec.reshape(1, self.embedding_size), self.W, metric=metric) \
            .reshape(len(self.node2idx))
        idx = distances.argsort()[1:n]

        return list(map(lambda i: self.idx2node[i], idx))

    def get_training_params(self) -> Dict[str, object]:
        assert self.train_config is not None
        return self.train_config

    def get_node2idx(self) -> Dict[int, int]:
        return self.node2idx

    def get_embeddings(self) -> np.ndarray:
        return self.W

    def save(self, dst_path: str, name_prefix: str) -> List[str]:
        weights_file = name_prefix + DeepWalk.WEIGHTS_NAME
        node2idx_file = name_prefix + DeepWalk.NODE2IDX_NAME

        with open(os.path.join(dst_path, node2idx_file), 'w') as f:
            json.dump(self.node2idx, f)
        np.savez(os.path.join(dst_path, weights_file), self.W)

        return [weights_file, node2idx_file]

    def load(self, dst_path: str, name_prefix: str) -> None:
        weights_file = name_prefix + DeepWalk.WEIGHTS_NAME
        node2idx_file = name_prefix + DeepWalk.NODE2IDX_NAME

        with open(os.path.join(dst_path, node2idx_file)) as f:
            tmp = json.load(f)
            self.node2idx = {int(k): int(v) for k, v in tmp.items()}
            self.idx2node = {v: k for k, v in self.node2idx.items()}

        arrays = np.load(os.path.join(dst_path, weights_file))
        self.W = arrays['arr_0']
