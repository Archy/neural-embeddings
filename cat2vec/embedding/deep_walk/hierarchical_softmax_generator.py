import logging
import time
from functools import reduce
from typing import Generator, List, Dict, Tuple

import huffman
import numpy as np

from cat2vec.graph.graph import Graph


class HierarchicalGenerator:
    """
    Generator for :class:`.hierarchical_softmax_deep_walk.DeepWalk`.
    Generator returns tuple of:
        - array of input words
        - indices of left (positive) nodes in the hierarchical softmax tree; padded to length of longest sequence in a batch
        - mask for each indices row; 1 means it's a proper node on path; 0 means its a dummy node used for padding only.
        - indices of right (negative) nodes in the hierarchical softmax tree; padded to length of longest sequence in a batch
        - mask for each indices row; same as for left indices mask
    """
    def __init__(self,
                 graph: Graph,
                 node2idx: Dict[int, int],
                 walks: List[List[int]],
                 window_size: int,
                 batch_size: int) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.graph = graph
        self.node2idx = node2idx
        self.walks = walks
        self.window_size = window_size
        self.batch_size = batch_size

        self.frequencies = _count_frequencies(graph, walks)
        self.indices = _create_softmax_tree(graph, self.frequencies)

    def generator(self) -> Generator:
        """
        :return: the actual generator
        """
        inputs = []
        left_indices = []
        right_indices = []

        batches_count = 0
        batch_time = time.time()
        total_time = 0

        np.random.shuffle(self.walks)
        for walk in self.walks:
            if len(walk) < 2:
                continue
            for i in range(2, len(walk)):
                for j in range(max(0, i - self.window_size), i):
                    tmp = [self.node2idx[walk[i]]]
                    tmp_left, tmp_right = self.indices[walk[j]]

                    inputs.append(tmp)
                    left_indices.append(tmp_left)
                    right_indices.append(tmp_right)

                    if len(inputs) >= self.batch_size:
                        total_time += time.time() - batch_time
                        self.logger.debug('Batch creation time: %f', (time.time() - batch_time))

                        left_padded, left_mask = _pad(left_indices)
                        right_padded, right_mask = _pad(right_indices)
                        yield (
                            np.array(inputs, dtype=np.int32),
                            left_padded,
                            left_mask,
                            right_padded,
                            right_mask
                        )
                        inputs = []
                        left_indices = []
                        right_indices = []
                        batches_count += 1
                        batch_time = time.time()
        self.logger.debug('Batches generated: %d in %f', batches_count, total_time)


def _pad(seq: List[np.ndarray]):
    """
    Pads each array in list to length of longest array.
    :return padded arrays as matrix + mask matrix
    """
    max_len = reduce(lambda x, y: max(y.shape[0], x), seq, 0)
    if max_len == 0:
        return np.zeros((len(seq), 1), dtype=np.float32), np.zeros((len(seq), 1, 1), dtype=np.float32)

    arr = np.full((len(seq), max_len), 0, dtype=np.int32)
    mask = np.zeros((len(seq), max_len, 1), dtype=np.float32)

    for i, s in enumerate(seq):
        arr[i, :s.shape[0]] = s[:]
        mask[i, :s.shape[0], 0] = 1

    return arr, mask


def _count_frequencies(graph: Graph, walks: List[List[int]]) -> Dict[int, int]:
    """
    Counts how many times each node appears in all random walks
    :return: mapping of nodes idx to their frequencies
    """
    freq = dict()
    for node in graph.nodes.values():
        freq[node.idx] = 1

    for walk in walks:
        for node in walk:
            freq[node] += 1
    return freq


def _huffman2idx(tree: huffman.Tree) -> Dict[str, int]:
    """
    :param tree: huffman tree
    :return: mapping of 0-1 sequences in huffman tree traversal to consecutive idx for indexing an array
    """
    huff2idx = {}

    def dfs(node: huffman.Node, seq: str, id_counter):
        if isinstance(node, huffman.Leaf):
            return id_counter
        huff2idx[seq] = id_counter
        id_counter = dfs(node.left, seq + '0', id_counter + 1)
        id_counter = dfs(node.right, seq + '1', id_counter)
        return id_counter

    dfs(tree.root, '', 0)  # root has no code
    return huff2idx


def _create_softmax_tree(graph: Graph, freq: Dict[int, int]) -> Dict[int, Tuple[np.ndarray, np.ndarray]]:
    """
    :param graph: a graph
    :param freq: frequency of each node in input data
    :return: dict mapping node to its left and right nodes in huffman tree
    """
    tree = huffman.Tree(freq.items())
    huff2idx = _huffman2idx(tree)
    node2huff = tree.codebook
    node2indices = {}

    for node in graph.nodes:
        code = node2huff[node]
        left_indices = []
        right_indices = []

        last = huff2idx['']
        for i in range(len(code)):
            if code[i] == '0':
                left_indices.append(last)
            else:
                right_indices.append(last)
            if i < len(code) - 1:
                last = huff2idx[str(code[0:i + 1])]

        node2indices[node] = (np.asarray(left_indices), np.asarray(right_indices))
    return node2indices
