import time
from typing import Generator, List, Dict, Tuple
import numpy as np

from cat2vec.graph.graph import Graph


class NegativeSamplesGenerator:
    """
    Generator for :class:`.deep_walk.DeepWalk`.
    Generator returns tuple of:
        - array of positive inputs
        - array of positive targets
        - array of negative inputs
        - array of negative targets
    """

    def __init__(self,
                 graph: Graph,
                 node2idx: Dict[int, int],
                 walks: List[List[int]],
                 window_size: int,
                 negative_samples_num: int,
                 batch_size: int) -> None:
        super().__init__()
        self.graph = graph
        self.node2idx = node2idx
        self.walks = walks
        self.window_size = window_size
        self.negative_samples_num = negative_samples_num
        self.np_nodes = np.asarray(list(graph.nodes.keys()), dtype=np.int32)
        self.batch_size = batch_size

    # https://stackoverflow.com/questions/42531143/type-hinting-generator-in-python-3-6
    def generator(self) -> Generator:
        """
        :return: the actual generator
        """
        pos_inputs = []
        pos_targets = []
        neg_inputs = []
        neg_targets = []

        batches_count = 0
        batch_time = time.time()
        total_time = 0

        np.random.shuffle(self.walks)
        for walk in self.walks:
            if len(walk) < 2:
                continue
            tmp_in, tmp_target = self._create_pos_samples(walk)
            pos_inputs += tmp_in
            pos_targets += tmp_target
            tmp_in, tmp_target = self._create_neg_samples(walk)
            neg_inputs += tmp_in
            neg_targets += tmp_target

            if len(pos_inputs) >= self.batch_size:
                total_time += time.time() - batch_time
                yield (
                    np.array(pos_inputs, dtype=np.int32),
                    np.array(pos_targets, dtype=np.int32),
                    np.array(neg_inputs, dtype=np.int32),
                    np.array(neg_targets, dtype=np.int32)
                )
                batches_count += 1
                pos_inputs = []
                pos_targets = []
                neg_inputs = []
                neg_targets = []
                batch_time = time.time()
        print('Batches generated: %d   (%f)' % (batches_count, total_time))

    def _create_pos_samples(self, walk: List[int]) -> Tuple[List[int], List[int]]:
        """
        Creates positive samples for given random walk. When getting context takes only nodes to the left
        :return: inputs words, corresponding positive words
        """
        inputs = []
        targets = []
        for i, node in enumerate(walk):
            if i < 1:
                continue
            tmp = list(map(lambda x: self.node2idx[x], walk[max(0, i - self.window_size):i]))
            targets += tmp
            inputs += [self.node2idx[node]] * len(tmp)
        assert len(inputs) == len(targets)
        return inputs, targets

    def _create_neg_samples(self, walk: List[int]) -> Tuple[List[int], List[int]]:
        """
        Creates negative samples for given random walk.
        :return: inputs words, corresponding negative words
        """
        inputs = []
        neg_targets = []

        for i, node in enumerate(walk):
            if i < 1:
                continue
            others = self.np_nodes[np.logical_not(np.isin(self.np_nodes, walk[max(0, i - self.window_size):i + 1]))]
            tmp = np.random.choice(others, self.negative_samples_num)
            neg_targets += list(map(lambda x: self.node2idx[x], tmp))
            inputs += [self.node2idx[node]] * len(tmp)
        assert len(inputs) == len(neg_targets)
        return inputs, neg_targets
