import json
import logging
import os
import time
import typing
from functools import wraps
from typing import Dict, List

import tensorflow as tf
import numpy as np

from sklearn.metrics.pairwise import pairwise_distances

from cat2vec.embedding.deep_walk.negative_samples_generator import NegativeSamplesGenerator
from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.utils.common import create_node_idx_mapping, create_initial_weight
from cat2vec.graph.graph import Graph


def train_wrapper(train_method):
    """
    Decorator that saves training config to dictionary
    """

    @wraps(train_method)
    def _save_train_config(self, *method_args, **method_kwargs):
        self.train_config = {
            'epochs': method_kwargs['epochs'],
            'batch_size': method_kwargs['batch_size'],
            'learning_rate': method_kwargs['learning_rate'],
            'window_size': method_kwargs['window_size'],
            'negative_samples_num': method_kwargs['negative_samples_num'],
            'use_glove_init': method_kwargs['use_glove_init'],
            'type': 'negative_sampling',
            'embeddings_size': self.embedding_size,
        }
        self.logger.info('Train config: %s', self.train_config)

        return train_method(self, *method_args, **method_kwargs)

    return _save_train_config


class DeepWalk(EmbeddingModel):
    """
    DeepWalk (skipgram + negative sampling based) implementation in tensorflow.
    See http://www.perozzi.net/publications/14_kdd_deepwalk.pdf for more.
    """
    NAME = 'deepWalk_negativeSampling_v1'
    WEIGHTS_NAME = '.weights.npz'
    NODE2IDX_NAME = '.node2idx.json'

    def __init__(self, embedding_size) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.embedding_size = embedding_size
        self.W = np.empty(shape=[0, embedding_size])
        self.node2idx = {}
        self.idx2node = {}
        self.train_config = None

    def get_name(self) -> str:
        return DeepWalk.NAME

    def fit_args(self, graph: Graph, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        return self.fit(
            graph=graph,
            walks=kwargs['walks'],
            epochs=epochs,
            batch_size=kwargs['batch_size'],
            learning_rate=learning_rate,
            window_size=kwargs['window_size'],
            negative_samples_num=kwargs['negative_samples_num'],
            use_glove_init=kwargs['use_glove_init'] if 'use_glove_init' in kwargs else False
        )

    @train_wrapper
    def fit(self, graph: Graph,
            walks: typing.List[typing.List[int]],
            epochs: int,
            batch_size: int,
            learning_rate: float,
            window_size: int,
            negative_samples_num: int,  # = 10
            use_glove_init: bool,
            ) -> typing.List[float]:
        tf.reset_default_graph()

        D = self.embedding_size
        self.node2idx = node2idx = create_node_idx_mapping(graph)
        self.idx2node = {v: k for k, v in node2idx.items()}

        # network input:
        samples_generator = NegativeSamplesGenerator(
            graph=graph,
            node2idx=node2idx,
            walks=walks,
            window_size=window_size,
            negative_samples_num=negative_samples_num,
            batch_size=batch_size
        )
        dataset = tf.data.Dataset.from_generator(samples_generator.generator,
                                                 output_types=(tf.int32, tf.int32, tf.int32, tf.int32))
        train_iterator = dataset.repeat(1).prefetch(batch_size * 100).make_initializable_iterator('train_iterator')
        tf_pos_input, tf_pos_output, tf_neg_input, tf_neg_output = train_iterator.get_next()

        # use shared weights:
        W = create_initial_weight(graph, node2idx, D, use_glove_init)
        tfW = tf.Variable(W, dtype=tf.float32)

        # positive context:
        tf_embedded_input = tf.nn.embedding_lookup(tfW, tf_pos_input)
        tf_embedded_output = tf.nn.embedding_lookup(tfW, tf_pos_output)
        pos_logits = tf.reduce_sum(tf_embedded_input * tf_embedded_output, axis=1)
        pos_loss = tf.nn.sigmoid_cross_entropy_with_logits(
            labels=tf.ones(tf.shape(pos_logits)), logits=pos_logits)

        # negative samples:
        tf_embedded_input = tf.nn.embedding_lookup(tfW, tf_neg_input)
        tf_embedded_output = tf.nn.embedding_lookup(tfW, tf_neg_output)
        neg_logits = tf.reduce_sum(tf_embedded_input * tf_embedded_output, axis=1)
        neg_loss = tf.nn.sigmoid_cross_entropy_with_logits(
            labels=tf.zeros(tf.shape(neg_logits)), logits=neg_logits)

        # total loss:
        loss = tf.reduce_mean(pos_loss) + tf.reduce_mean(neg_loss)
        tf.summary.scalar('loss', loss)
        train_op = tf.train.AdamOptimizer(learning_rate).minimize(loss)

        init = tf.global_variables_initializer()
        costs = []
        total_time_start = time.time()
        with tf.Session() as session:
            session.run(init)

            # merge all summaries
            merged = tf.summary.merge_all()
            summary_writer = tf.summary.FileWriter(self.get_tensorboard_output_dir(), session.graph)

            self.logger.info('Starting training')

            for epoch in range(epochs):
                session.run(train_iterator.initializer)
                start_time = time.time()

                cost = 0
                try:
                    while True:
                        _, c, summary = session.run(
                            (train_op, loss, merged)
                        )
                        cost += c
                except tf.errors.OutOfRangeError:
                    pass

                costs.append(cost)
                summary_writer.add_summary(summary, epoch)
                self.logger.info('Epoch %d:\t%f\t(%f)', epoch, cost, time.time() - start_time)

            self.W = session.run(tfW)

        self.logger.info('Training finished (%f)' % (time.time() - total_time_start))
        return costs

    def predict(self, node_idx: int, n: int, metric: str) -> typing.List[int]:
        node_vec = self.W[self.node2idx[node_idx]]
        distances = pairwise_distances(node_vec.reshape(1, self.embedding_size), self.W, metric=metric) \
            .reshape(len(self.node2idx))
        idx = distances.argsort()[1:n]

        return list(map(lambda i: self.idx2node[i], idx))

    def get_training_params(self) -> Dict[str, object]:
        assert self.train_config is not None
        return self.train_config

    def get_node2idx(self) -> Dict[int, int]:
        return self.node2idx

    def get_embeddings(self) -> np.ndarray:
        return self.W

    def save(self, dst_path: str, name_prefix: str) -> List[str]:
        weights_file = name_prefix + DeepWalk.WEIGHTS_NAME
        node2idx_file = name_prefix + DeepWalk.NODE2IDX_NAME

        with open(os.path.join(dst_path, node2idx_file), 'w') as f:
            json.dump(self.node2idx, f)
        np.savez(os.path.join(dst_path, weights_file), self.W)

        return [weights_file, node2idx_file]

    def load(self, dst_path: str, name_prefix: str) -> None:
        weights_file = name_prefix + DeepWalk.WEIGHTS_NAME
        node2idx_file = name_prefix + DeepWalk.NODE2IDX_NAME

        with open(os.path.join(dst_path, node2idx_file)) as f:
            tmp = json.load(f)
            self.node2idx = {int(k): int(v) for k, v in tmp.items()}
            self.idx2node = {v: k for k, v in self.node2idx.items()}

        arrays = np.load(os.path.join(dst_path, weights_file))
        self.W = arrays['arr_0']
