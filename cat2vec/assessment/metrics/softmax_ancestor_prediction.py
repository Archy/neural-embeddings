import logging
import time

import numpy as np
from typing import Dict

from sklearn.model_selection import train_test_split

from cat2vec.graph.graph import Graph
from cat2vec.embedding.embedding_model import EmbeddingModel

import tensorflow as tf


logger = logging.getLogger(__name__)
CLEAR_TENSORFLOW = True


def assess_ancestor_with_softmax(graph: Graph, embedding_model: EmbeddingModel,
                                 epochs: int, learning_rate: float, batch_size: int):
    """
    Creates simple softmax model that learns to predict common parent of 2 nodes.
    Only correct samples (always 2 sibling nodes and their parents) are used.

    :return: dictionary that contains last training epoch loss and accuracy and evaluation loss and accuracy
    """
    # Good config:
    # learning_rate = 0.001
    # batch_size = 128
    # epochs=10
    if CLEAR_TENSORFLOW:
        tf.keras.backend.clear_session()
        tf.reset_default_graph()

    V = len(graph.nodes)
    W = embedding_model.get_embeddings()
    model = _create_model(V, W, learning_rate)
    X_train, X_test, y_train, y_test = _prepare_data(graph, embedding_model.get_node2idx())

    logger.info('Starting training')
    start_time = time.time()
    tensorboard_log_dir = embedding_model.get_tensorboard_output_dir() + '_ancestor_prediciton_accuracy'
    history = model.fit(X_train, y_train,
                        batch_size=batch_size,
                        epochs=epochs,
                        callbacks=[
                            tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_dir, histogram_freq=0,
                                                           write_graph=False, update_freq='epoch'),
                        ])
    logger.info('Training finished (%f)' % (time.time() - start_time))
    metrics = model.evaluate(X_test, y_test, batch_size=batch_size)

    results = {
        'training_loss': history.history['loss'][-1],
        'training_accuracy': history.history['acc'][-1],
        'test_loss': metrics[0],
        'test_accuracy': metrics[1]
    }
    return results


def _create_model(V: int, W: np.ndarray, learning_rate: float):
    logging.info('Creating ancestor prediction model')
    embedding_size = W.shape[1]

    input_layer = tf.keras.layers.Input(shape=(2,), dtype=tf.int32)
    embedding_layer = tf.keras.layers.Embedding(input_dim=V, output_dim=embedding_size, weights=[W], input_length=2,
                                                trainable=False, dtype=tf.float32)(input_layer)
    flattened = tf.keras.layers.Flatten()(embedding_layer)
    output = tf.keras.layers.Dense(units=V, activation='softmax')(flattened)

    optimizer = tf.keras.optimizers.Adam(lr=learning_rate, decay=0.001)
    model = tf.keras.models.Model(
        inputs=[input_layer],
        outputs=[output]
    )
    model.compile(
        optimizer=optimizer,
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy']
    )

    logger.info(model.summary())
    return model


def _prepare_data(graph: Graph, node2idx: Dict[int, int]):
    logger.info('Preparing data')
    X = []
    y = []

    for node in graph.nodes.values():
        if len(node.children) < 2:
            continue
        parent_id = node2idx[node.idx]

        for child_1 in node.children:
            child_1_id = node2idx[child_1.idx]

            for child_2 in node.children:
                child_2_id = node2idx[child_2.idx]

                if child_1_id == child_2_id:
                    continue
                X.append((child_1_id, child_2_id))
                y.append(parent_id)
    X = np.asarray(X)
    y = np.asarray(y)

    return train_test_split(X, y, test_size=0.5)
