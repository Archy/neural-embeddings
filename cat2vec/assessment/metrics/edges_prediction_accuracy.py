import json
import logging
from typing import Tuple, Dict, Optional, Set, List, Any

import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.graph.graph import Graph

logger = logging.getLogger(__name__)


"""
Map of operations for creating an edge features vector from its nodes vectors
"""
EDGE_FEATURES_OPERATORS = {
    'AVERAGE': lambda n1, n2: (n1 + n2) / 2,
    'HADAMARD': lambda n1, n2: n1 * n2
}

MAX_PRINTED_ERRONEOUS_EDGES = 100
PRINTED_EDGES_VECTORS = 2


def assess_edges_prediction_accuracy(graph: Graph,
                                     model: EmbeddingModel,
                                     operator: str,
                                     negative_edges_per_node: int,
                                     print_erroneous_edges: bool,
                                     removed_edges: Optional[Set[Tuple[int, int]]] = None,
                                     added_edges: Optional[Set[Tuple[int, int]]] = None
                                     ) -> Dict[str, Any]:
    """
    Calculates (existing and nonexistent) edges features vectors similar way as in  node2vec paper.
    Then training simple logistic regression model and returns its validation accuracy.

    :param graph: a graph
    :param model: model to asses
    :param operator: function that takes nodes features vectors and create edge feature vector
    :param negative_edges_per_node: nonexistent edges features vectors as numpy matrix
    :param print_erroneous_edges: whether erroneously predicted edges (added or missing) should be logged
    :param removed_edges: [optional] edges removed before training. Will check if there were restored by created embedding
    :param added_edges: [optional] edges added before training. Will check if there were removed by created embedding
    :return:
    """
    node2idx = model.get_node2idx()
    W = model.get_embeddings()

    def _create_data(edges, labels, edges_nodes):
        _x_train, _x_test, _y_train, _y_test, _, _test_edges_nodes = \
            train_test_split(edges, labels, edges_nodes, test_size=0.5)
        return _x_train, _x_test, _y_train, _y_test, _test_edges_nodes

    x_train_pos, x_test_pos, y_train_pos, y_test_pos, test_edges_nodes_pos = \
        _create_data(*_create_existing_edges_features(graph, node2idx, W, operator, added_edges))
    x_train_neg, x_test_neg, y_train_neg, y_test_neg, test_edges_nodes_neg = \
        _create_data(*_create_missing_edges_features(graph, node2idx, W, operator, negative_edges_per_node, removed_edges))

    x_train = np.concatenate([x_train_pos, x_train_neg])
    y_train = np.concatenate([y_train_pos, y_train_neg])
    x_train, y_train = _shuffle(x_train, y_train)

    # model
    logistic_reg = LogisticRegression(solver='liblinear')
    logistic_reg.fit(x_train, y_train)

    pos_score = logistic_reg.predict(x_test_pos)
    neg_score = logistic_reg.predict(x_test_neg)
    results = _count_accuracy(pos_score, y_test_pos, neg_score, y_test_neg)

    if print_erroneous_edges:
        _print_erroneous_edges(graph, pos_score, test_edges_nodes_pos, neg_score, test_edges_nodes_neg)
    if removed_edges is not None:
        results['Restored edges'] = _count_restored_edges(logistic_reg, removed_edges, graph, node2idx, W, operator)
    if added_edges is not None:
        results['Filtered edges'] = _count_filtered_edges(logistic_reg, added_edges, graph, node2idx, W, operator)

    logger.info('Edges prediction accuracy (%s): %s', operator, json.dumps(results))
    return results


def _count_accuracy(pos_score: np.ndarray, pos_labels: np.ndarray,
                    neg_score: np.ndarray, neg_labels: np.ndarray) -> Dict[str, Any]:
    """
    Counts confusion matrix and recall
    """
    true_total = len(pos_labels)
    true_positive = np.sum(pos_score == pos_labels)
    false_negative = true_total - true_positive

    false_total = len(neg_labels)
    true_negative = np.sum(neg_score == neg_labels)
    false_positive = false_total - true_negative

    precision = true_positive / (true_positive + false_positive)
    recall = true_positive / (true_positive + false_negative)
    f1 = 2 * precision * recall / (precision + recall)

    return {
        'True positive':  '{} ({})'.format(true_positive, true_positive / true_total),
        'False negative': '{} ({})'.format(false_negative, false_negative / true_total),
        'True negative': '{} ({})'.format(true_negative, true_negative / false_total),
        'False positive': '{} ({})'.format(false_positive, false_positive / false_total),
        'Precision': precision,
        'Recall': recall,
        'F1': f1
    }


def _print_erroneous_edges(graph,
                           pos_scores: np.ndarray, test_edges_nodes_pos: List[Tuple[int, int]],
                           neg_score: np.ndarray, test_edges_nodes_neg: List[Tuple[int, int]]) -> None:
    """
    Prints edges (as source node - destination node) that have been predicted incorrectly: either are false positive or
    false negative. Prints only first MAX_PRINTED_ERRONEOUS_EDGES erroneous edges.
    """
    def _print_internal(expected_score, scores, edges_nodes, error_msg, limit_msg):
        printed = 0
        for i, score in enumerate(scores):
            assert score == 0 or score == 1
            if score != expected_score:
                src_node, dst_node = edges_nodes[i]
                logger.info(error_msg, graph.nodes[src_node].name, graph.nodes[dst_node].name)
                printed += 1
            if printed > MAX_PRINTED_ERRONEOUS_EDGES:
                logger.warning(limit_msg, MAX_PRINTED_ERRONEOUS_EDGES)
                break

    _print_internal(1, pos_scores, test_edges_nodes_pos,
                    '\tMissing edge from \"%s\" to \"%s\"',
                    'Number of missing edges exceeded max erroneous edges to print (%d). Stopping printing now')
    _print_internal(0, neg_score, test_edges_nodes_neg,
                    '\tWrong existing edge from \"%s\" to \"%s\"',
                    'Number of wrong edges exceeded max erroneous edges to print (%d). Stopping printing now')


def _count_restored_edges(logistic_reg,
                          removed_edges: Optional[Set[Tuple[int, int]]],
                          graph: Graph,
                          node2idx: Dict[int, int],
                          W: np.ndarray,
                          operator: str) -> str:
    """
    Counts number of edges that were removed before training and have been restored by created embeddings
    """
    logger.info('Checking if edges removed before training have been restored')
    restored_edges = 0
    for parent_id, child_id in removed_edges:
        assert graph.nodes[child_id] not in graph.nodes[parent_id].children
        parent_features = W[node2idx[parent_id]]
        child_features = W[node2idx[child_id]]

        edge_features = EDGE_FEATURES_OPERATORS[operator](parent_features, child_features)
        prediction = logistic_reg.predict(edge_features.reshape(1, -1))

        if prediction == 1:
            restored_edges += 1
            logger.info('\tEdge from \"%s\" to \"%s\" has been restored', graph.nodes[parent_id].name,
                        graph.nodes[child_id].name)
        else:
            logger.info('\t\tEdge from \"%s\" to \"%s\" has NOT been restored', graph.nodes[parent_id].name,
                        graph.nodes[child_id].name)
    logger.info('Restored edges: %d out of %d', restored_edges, len(removed_edges))
    return '{} / {} ({})'.format(restored_edges, len(removed_edges), restored_edges / len(removed_edges))


def _count_filtered_edges(logistic_reg,
                          added_edges: Optional[Set[Tuple[int, int]]],
                          graph: Graph,
                          node2idx: Dict[int, int],
                          W: np.ndarray,
                          operator: str) -> str:
    """
    Counts number of edges that were added before training and have been filtered out by created embeddings
    """
    logger.info('Checking if edges added before training have been removed')
    removed_edges = 0
    for parent_id, child_id in added_edges:
        assert graph.nodes[child_id] in graph.nodes[parent_id].children

        parent_features = W[node2idx[parent_id]]
        child_features = W[node2idx[child_id]]

        edge_features = EDGE_FEATURES_OPERATORS[operator](parent_features, child_features)
        prediction = logistic_reg.predict(edge_features.reshape(1, -1))

        if prediction == 0:
            removed_edges += 1
            logger.info('\tEdge from \"%s\" to \"%s\" has been filtered out', graph.nodes[parent_id].name,
                        graph.nodes[child_id].name)
        else:
            logger.info('\t\tEdge from \"%s\" to \"%s\" has NOT been filtered out', graph.nodes[parent_id].name,
                        graph.nodes[child_id].name)
    logger.info('Filtered edges: %d out of %d', removed_edges, len(added_edges))
    return '{} / {}, ({})'.format(removed_edges, len(added_edges), removed_edges / len(added_edges))


def _create_existing_edges_features(graph: Graph,
                                    node2idx: Dict[int, int],
                                    W: np.ndarray,
                                    operator: str,
                                    added_edges: Optional[Set[Tuple[int, int]]]) -> Tuple[np.ndarray, np.ndarray, List[Tuple[int, int]]]:
    """
    Creates features vectors for every existing edge in a graph

    :param graph: a graph
    :param node2idx: dict mapping nodes idx to consecutive indices in an W array
    :param W: nodes features vectors as numpy matrix
    :param operator: function that takes nodes features vectors and create edge feature vector
    :return: existing edges features vectors as numpy matrix
    """
    edges = []
    edges_nodes = []
    for node in graph.nodes.values():
        node_features = W[node2idx[node.idx]]

        for c in node.children:
            if added_edges is not None and (node.idx, c.idx) in added_edges:
                continue  # don't use edge that has been added to the graph before training

            c_features = W[node2idx[c.idx]]

            edge_features = EDGE_FEATURES_OPERATORS[operator](node_features, c_features)
            edges.append(edge_features)

            edges_nodes.append((node.idx, c.idx))

    # print some edges features vectors
    logger.debug('Printing sample existing edges vectors')
    for i in range(PRINTED_EDGES_VECTORS):
        logger.debug(edges[i])

    labels = np.ones(len(edges))
    return np.array(edges, dtype=np.float32), labels, edges_nodes


def _create_missing_edges_features(graph: Graph,
                                   node2idx: Dict[int, int],
                                   W: np.ndarray,
                                   operator: str,
                                   negative_edges_per_node: int,
                                   removed_edges: Optional[Set[Tuple[int, int]]]) -> Tuple[np.ndarray, np.ndarray, List[Tuple[int, int]]]:
    """
    Creates features vectors for given number of nonexistent edges in a graph

    :param graph: a graph
    :param node2idx: dict mapping nodes idx to consecutive indices in an W array
    :param W: nodes features vectors as numpy matrix
    :param operator: function that takes nodes features vectors and create edge feature vector
    :param negative_edges_per_node: nr of nonexistent edges to be taken for each node
    :return: nonexistent edges features vectors as numpy matrix
    """
    edges = []
    edges_nodes = []
    np_nodes = np.asarray(list(graph.nodes.keys()), dtype=np.int32)
    all_nodes_neighbours = dict()

    # create sets of all neighbours of edges including their parents
    for node in graph.nodes.values():
        all_nodes_neighbours[node.idx] = set()
        all_nodes_neighbours[node.idx].add(node.idx)
    for node in graph.nodes.values():
        for c in node.children:
            all_nodes_neighbours[node.idx].add(c.idx)
            all_nodes_neighbours[c.idx].add(node.idx)

    for node in graph.nodes.values():
        node_features = W[node2idx[node.idx]]

        others = np_nodes[np.logical_not(np.isin(np_nodes, all_nodes_neighbours[node.idx]))]
        others = np.random.choice(others, min(negative_edges_per_node, len(others)))
        for o in others:
            if removed_edges is not None and (node.idx, o) in removed_edges:
                continue  # don't use edge that has been removed from the graph before training

            o_features = W[node2idx[o]]

            negative_edge = EDGE_FEATURES_OPERATORS[operator](node_features, o_features)
            edges.append(negative_edge)

            edges_nodes.append((node.idx, o))

    # print some edges features vectors
    logger.debug('Printing sample missing edges vectors')
    for i in range(PRINTED_EDGES_VECTORS):
        logger.debug(edges[i])

    labels = np.zeros(len(edges))
    return np.array(edges, dtype=np.float32), labels, edges_nodes


def _shuffle(x, y):
    """
    Shuffles 2 numpy arrays the same way
    :return: shuffeled arrays as tuple (x_shuffled, y_shuffled)
    """
    shuffled_indices = np.random.permutation(len(x))
    return x[shuffled_indices], y[shuffled_indices]
