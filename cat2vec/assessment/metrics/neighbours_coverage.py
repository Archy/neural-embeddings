import logging
import statistics
from typing import Dict, Union

from cat2vec.graph.graph import Graph, Node

logger = logging.getLogger(__name__)

METRICS = ['cosine', 'euclidean']


def assess_neighbours_coverage(graph: Graph, model) -> Dict[str, Dict[str, Union[int, float]]]:
    """
    Counts how many predicted neighbours of a node are it's true neighbours
    """
    root = _find_root(graph)
    results = {}

    for metric in METRICS:
        results[metric] = _assess_neighbours_coverage(root, model, metric)

    return results


def _assess_neighbours_coverage(root: Node, model, metric: str):
    logger.info('Assessing metric: %s', metric)
    total_links = 0
    correct = 0
    precisions = []

    parsed = set()
    q = [root]
    while q:
        node = q.pop(0)
        if node.idx not in parsed:
            parsed.add(node.idx)

            links = len(node.children)
            if links > 0:
                total_links += links

                neighbours = model.predict(node.idx, links, metric)
                children = set(map(lambda x: x.idx, node.children))

                node_correct = 0
                for n in neighbours:
                    if n in children:
                        node_correct += 1
                correct += node_correct

                node_precision = node_correct / links
                precisions.append(node_precision)

                for c in node.children:
                    q.append(c)

    precision_std_dev = statistics.stdev(precisions)
    precision_mean = statistics.mean(precisions)
    precision_median = statistics.median(precisions)

    logger.info('Neighbours coverage: %d / %d', correct, total_links)
    logger.info('Neighbours coverage mean: %f', precision_mean)
    logger.info('Neighbours coverage median: %f', precision_median)
    logger.info('Neighbours coverage std dev: %f', precision_std_dev)

    results = {
        'Predicted neighbours': correct,
        'Total neighbours': total_links,
        'Prediction mean': precision_mean,
        'Prediction median': precision_median,
        'Prediction std dev': precision_std_dev
    }
    return results


def _find_root(graph: Graph) -> Node:
    """
    :param graph: rooted graph
    :return: root node
    """
    nodes = set(graph.nodes.keys())
    for n in graph.nodes.values():
        nodes -= set(map(lambda c: c.idx, n.children))

    logger.debug('Graph root node(s): %s', nodes)
    assert (len(nodes) == 1)

    root = graph.nodes[nodes.pop()]
    logger.debug('Graph root: %s', root)
    return root
