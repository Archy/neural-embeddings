import logging
from enum import Enum
from random import randrange, randint
from typing import Dict, Tuple, List, Optional

import numpy as np
from sklearn.metrics import pairwise_distances

from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.graph.graph import Graph, Node


logger = logging.getLogger(__name__)


class AncestorPredictionType(Enum):
    """
    How to predict the common ancestor node
    """
    SOFTMAX_PREDICTOR = 1
    NEAREST_NEIGHBOUR = 2


"""
Map of operations for creating a parent feature vector. Used by NEAREST_NEIGHBOUR predictor
"""
ANCESTOR_VECTOR_OPERATORS = {
    'MEAN': lambda n1, n2: (n1 + n2) / 2,
    'HADAMARD': lambda n1, n2: n1 * n2,
    'SQRT': lambda n1, n2: np.sqrt(n1 ** 2 + n2 ** 2),
    'SQRT_2': lambda n1, n2: np.sqrt((n1 - n2)**2),
    'WEIGHTED_L1': lambda n1, n2: np.abs(n1 - n2),
    'WEIGHTED_L2': lambda n1, n2: np.abs(n1 - n2)**2
}


def assess_ancestor_by_distance(graph: Graph, model: EmbeddingModel, samples_per_node: int,
                                max_distance: int, k: int, operators: List[str] = None) -> Dict:
    """
    Assess if it's possible to predict first common ancestor of two random nodes.
    Takes top-k predictions and checks if common ancestor is in that set.

    :param graph: the graph
    :param model: model with embeddings
    :param operators: one of ANCESTOR_VECTOR_OPERATORS to use
    :param samples_per_node: number of cases to generate per each node
    :param max_distance: max distance between ancestor and child node
    :param k: number of top-k prediction to use
    :return:
    """
    node2idx = model.get_node2idx()
    idx2node = {v: k for k, v in node2idx.items()}

    embeddings = model.get_embeddings()
    test_cases = create_samples(graph, samples_per_node, max_distance)
    operators = operators if operators is not None else ANCESTOR_VECTOR_OPERATORS.keys()

    results = {
        'k': k,
        'test_cases': len(test_cases)
    }
    for operator_name in operators:
        logger.info('Assessing %s', operator_name)
        operator = ANCESTOR_VECTOR_OPERATORS[operator_name]
        top_1 = 0
        top_k = 0

        for case in test_cases:
            first_node_vector = embeddings[node2idx[case[1]]]
            second_node_vector = embeddings[node2idx[case[2]]]

            predicted_ancestor_vector = operator(first_node_vector, second_node_vector)
            distances = pairwise_distances(
                predicted_ancestor_vector.reshape(1, embeddings.shape[1]), embeddings, metric='cosine') \
                .reshape(len(node2idx))
            predicted_nodes = list(map(lambda i: idx2node[i], distances.argsort()[1:k]))

            ancestor_idx = case[0]
            if predicted_nodes[0] == ancestor_idx:
                top_1 += 1
            if ancestor_idx in predicted_nodes:
                top_k += 1

        top_1 /= len(test_cases)
        top_k /= len(test_cases)
        logger.info('Ancestor prediction accuracy (%s): top_1: %f top_%d: %f', operator_name, top_1, k, top_k)
        results[operator_name] = {
            'top_1': top_1,
            'top_k': top_k
        }

    return results


def create_samples(graph: Graph, samples_per_node: int, max_distance: int) -> List[Tuple[int, int, int]]:
    """
    Creates test cases for testing ancestor prediction for 2 nodes.

    :param graph: the graph
    :param samples_per_node: nr of test cases per each node
    :param max_distance: max distance from ancestor to some child node
    :return:
    """
    samples = []
    logger.info('Creating ancestor - child nodes samples')

    for node in graph.nodes.values():
        for i in range(samples_per_node):
            first_distance = randint(1, max_distance)
            second_distance = randint(1, max_distance)
            sample = _create_sample(node, first_distance, second_distance)
            if sample is not None:
                samples.append(sample)

    logger.info('Created %d samples', len(samples))
    return samples


def _create_sample(node: Node, first_distance: int, second_distance: int) -> Optional[Tuple[int, int, int]]:
    """
    :param node: ancestor node
    :param first_distance: distance from ancestor node to some child
    :param second_distance: distance from ancestor node to another child
    :return: ancestor node idx, first child idx, second child idx
    """
    def random_walk(curr_node, distance):
        if distance == 0 or len(curr_node.children) == 0:
            return curr_node.idx
        next_node = curr_node.children[randrange(0, len(curr_node.children))]
        return random_walk(next_node, distance-1)

    child_nodes = list(node.children)
    if len(child_nodes) < 2:
        return None
    first_start = child_nodes[randrange(0, len(child_nodes))]
    child_nodes.remove(first_start)
    second_start = child_nodes[randrange(0, len(child_nodes))]
    assert first_start != second_start

    return node.idx, first_start.idx, second_start.idx
