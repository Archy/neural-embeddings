import csv
import datetime
import json
import os
import logging
from typing import Dict

from cat2vec.assessment.assessment_saver.assessment_saver import AssessmentSaver
from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.utils.json_utils import EnumEncoderJsonAware


class LocalAssessmentSaver(AssessmentSaver):
    """
    Saves model to local directory and assessments results to local csv file
    """

    def __init__(self, cvs_path, dst_path) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.cvs_path = cvs_path
        self.dst_path = dst_path

        self.next_id = 1
        if os.path.exists(cvs_path):
            with open(cvs_path) as cvs_file:
                self.next_id = sum(1 for _ in cvs_file) + 1

    def save(self, model: EmbeddingModel, assessment_results: Dict):
        prefix = model.get_name() + '.' + str(self.next_id)

        self.logger.info('Saving model to %s with prefix %s', self.dst_path, prefix)
        model.save(self.dst_path, prefix)

        data = {
            'train_config': model.get_training_params(),
            'assessment_results': assessment_results
        }
        data_path = os.path.join(self.dst_path, prefix+'.data.json')
        self.logger.info('Saving data to %s', data_path)
        with open(data_path, mode='w') as data_file:
            json.dump(data, data_file, cls=EnumEncoderJsonAware)

        self.logger.debug('Adding entry to csv')
        with open(self.cvs_path, mode='a') as csv_file:
            writer = csv.writer(csv_file, delimiter=',',  quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow([self.next_id, model.get_name(), prefix, datetime.datetime.now()])

        self.next_id += 1
