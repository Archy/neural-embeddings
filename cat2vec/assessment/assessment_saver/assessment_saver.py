from abc import ABCMeta, abstractmethod
from typing import Dict

from cat2vec.embedding.embedding_model import EmbeddingModel


class AssessmentSaver:
    __metaclass__ = ABCMeta

    @abstractmethod
    def save(self, model: EmbeddingModel, assessment_results: Dict) -> None:
        """
        Saves model, training config and assessment results.
        """
        raise NotImplementedError


# Other ideas:
# AWS dynamoDB + s3 / other external database (like MongoDB)
# https://datascience.stackexchange.com/questions/19802/best-practices-to-store-python-machine-learning-models
