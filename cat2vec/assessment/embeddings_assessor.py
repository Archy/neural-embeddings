import random
from typing import List, Tuple, Dict, Optional, Set

import logging
import time

import numpy as np

import cat2vec.graph.loader as loader
from matplotlib import pyplot as plt
from enum import Enum
from sklearn.manifold import TSNE

from cat2vec.assessment.metrics.softmax_ancestor_prediction import assess_ancestor_with_softmax
from cat2vec.assessment.assessment_saver.assessment_saver import AssessmentSaver
from cat2vec.assessment.metrics.ancestor_prediction import assess_ancestor_by_distance
from cat2vec.assessment.metrics.edges_prediction_accuracy import EDGE_FEATURES_OPERATORS, \
    assess_edges_prediction_accuracy
from cat2vec.assessment.metrics.neighbours_coverage import assess_neighbours_coverage
from cat2vec.embedding.embedding_model import EmbeddingModel
from cat2vec.graph.graph import Graph
from cat2vec.walks import random_walk


class Metrics(Enum):
    NEIGHBOURS_COVERAGE = 1
    EDGES_PREDICTION = 2
    PRINT_NEIGHBOURS = 3
    TSNE_VISUALIZATION = 4
    ANCESTOR_BY_DISTANCE = 5
    ANCESTOR_WITH_SOFTMAX = 6


class EmbeddingsAssessor:
    """
    Class for training and assessing neural embeddings for graphs
    """
    def __init__(self,
                 model: EmbeddingModel,
                 assessment_saver: AssessmentSaver,
                 cat_path: str, dict_path: str, art_filepath: str = None, root_node: str = None) -> None:
        """
        :param model: grapah embedding model to assess
        :param assessment_saver: results of assessment will be passed to this object for saving.
        :param cat_path: path to file named like: history-of-poland-10-cats_links-en-20111201
        :param dict_path: path to file named like: history-of-poland-10-po_linkach-cats_dict-en-20111201
        :param art_filepath: path to file named like: pl_ww2-po_linkach-cats_dict-en-20111201
        :param root_node: graph's root node name. Must be passed if there might be disconnected subgraphs in the graph
        """
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.model: EmbeddingModel = model
        self.assessment_saver = assessment_saver
        self.graph = loader.load_graph(cat_path, dict_path, art_filepath=art_filepath, root=root_node)
        self.walks_memento = _WalksMemento(self.graph)
        self.last_results = None

        self.assessment_dict = {
            Metrics.NEIGHBOURS_COVERAGE: self._assess_neighbours_coverage,
            Metrics.EDGES_PREDICTION: self._assess_edges_prediction,
            Metrics.PRINT_NEIGHBOURS: self._print_closest_neighbours,
            Metrics.TSNE_VISUALIZATION: self._create_tsne_visualization,
            Metrics.ANCESTOR_BY_DISTANCE: self._assess_ancestor_prediction_by_distance,
            Metrics.ANCESTOR_WITH_SOFTMAX: self._assess_ancestor_prediction_with_softmax
        }
        self.removed_edges = None
        self.added_edges = None

    def fit(self, learning_rate: float, epochs: int, **kwargs) -> List[float]:
        """
        Trains model

        :param learning_rate: learning rate
        :param epochs: epochs
        :param kwargs: additional config. To generate random walks and pass them to graphs, you need to specify
             walks_per_node, walk_length and restart_probability here
        :return: list of losses for each epoch
        """
        walks = self.walks_memento.get_random_walks(**kwargs)
        if walks is not None:
            self.logger.info('Training \'%s\' with random walks', self.model.get_name())
            start = time.time()
            losses = self.model.fit_args(self.graph, learning_rate, epochs, walks=walks, **kwargs)
            end = time.time()
            self.logger.info('Training took %f', end - start)
        else:
            self.logger.info('Training \'%s\' without random walks', self.model.get_name())
            start = time.time()
            losses = self.model.fit_args(self.graph, learning_rate, epochs, **kwargs)
            end = time.time()
            self.logger.info('Training took %f', end-start)
        return losses

    def prefit_assessment_preparation(self, config: 'AssessmentConfig') -> None:
        config.prepare_graph(self.graph)

    def assess_model(self, save_results: bool, config: 'AssessmentConfig') -> Dict:
        """
        Assess model with specified metrics
        :param save_results: if model and assessments result should be saved
        :param config: metrics with config to use
        :return: dictionary with metrics results
        """
        self.logger.info('Assessing \'%s\' using metrics: %s', self.model.get_name(), list(config.metrics.keys()))

        results = {}
        for m in config.metrics.keys():
            metric_results = self.assessment_dict[m](**config.metrics[m])
            if metric_results is not None:
                results[m.name] = metric_results

        if save_results:
            self.save_results(results)

        return results

    def fit_assess(self,  learning_rate: float, epochs: int, save_results: bool,
                   config: 'AssessmentConfig', **kwargs) -> Tuple[List[float], Dict]:
        """
        Trains and assess model. See fit and assess_model for more info.
        """
        losses = self.fit(learning_rate, epochs, **kwargs)
        results = self.assess_model(save_results, config)
        return losses, results

    def load_model(self, src_path, prefix):
        """
        Loads model from memory
        """
        self.model.load(src_path, prefix)

    def save_results(self, results: Dict):
        """
        Saves results and model
        """
        if self.assessment_saver is not None:
            self.assessment_saver.save(self.model, results)

    def _assess_neighbours_coverage(self):
        return assess_neighbours_coverage(self.graph, self.model)

    def _assess_edges_prediction(self, negative_edges_per_node: int,
                                 print_erroneous_edges: bool,
                                 removed_edges: Optional[Set[Tuple[int, int]]],
                                 added_edges: Optional[Set[Tuple[int, int]]]):
        results = {}
        for operator in EDGE_FEATURES_OPERATORS.keys():
            operator_results = assess_edges_prediction_accuracy(self.graph, self.model,
                                                                operator, negative_edges_per_node,
                                                                print_erroneous_edges,
                                                                removed_edges, added_edges)
            results[operator] = operator_results
        return results

    def _print_closest_neighbours(self, test_no: int, neighbors_no: int):
        self.logger.info('Printing closest neighbours')
        for t in list(random.sample(self.graph.nodes.keys(), k=test_no)):
            closest = self.model.predict(t, neighbors_no)

            self.logger.info(self.graph.nodes[t].name)
            for c in closest:
                self.logger.info('\t%s', self.graph.nodes[c].name)
        return None  # no results to save, only print

    def _create_tsne_visualization(self, image_size: Tuple[int, int], dpi: int, dst_path: str):
        self.logger.info('Creating TSNE visualization')
        embeddings = self.model.get_embeddings().copy()
        embeddings2D = TSNE(n_components=2).fit_transform(embeddings)
        self.logger.debug('Embeddings after tsne reduction: %s', embeddings2D.shape)

        plt.figure(num=None, figsize=image_size, dpi=dpi)
        plt.scatter(embeddings2D[:, 0], embeddings2D[:, 1], s=2)

        for i, node in self.graph.nodes.items():
            idx = self.model.get_node2idx()[i]
            plt.annotate(node.name, embeddings2D[idx, :], size=2)

        plt.savefig(dst_path)
        return {
            'path': dst_path
        }

    def _assess_ancestor_prediction_by_distance(self, samples_per_node: int,  max_distance: int, k: int, operators: List[str]):
        return assess_ancestor_by_distance(self.graph, self.model, samples_per_node, max_distance, k, operators)

    def _assess_ancestor_prediction_with_softmax(self, epochs: int, learning_rate: float, batch_size: int):
        return assess_ancestor_with_softmax(self.graph, self.model, epochs, learning_rate, batch_size)


class AssessmentConfig:
    """
    Configuration of embedding assessment
    """

    def __init__(self) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.metrics = {}
        self.edges_to_remove = 0
        self.edges_to_add = 0

    def with_neighbours_coverage(self) -> 'AssessmentConfig':
        self.metrics[Metrics.NEIGHBOURS_COVERAGE] = {}
        return self

    def with_edges_prediction(self, negative_edges_per_node: int,
                              print_erroneous_edges: bool = True,
                              edges_to_add: int = 0,
                              edges_to_remove: int = 0) -> 'AssessmentConfig':
        if edges_to_remove + edges_to_add == 0:
            # no additional graph preparation needed
            self.metrics[Metrics.EDGES_PREDICTION] = {
                'negative_edges_per_node': negative_edges_per_node,
                'print_erroneous_edges': print_erroneous_edges,
                'removed_edges': None,
                'added_edges': None
            }
        else:
            # removed_edges and added_edges will be calculated and added in prefit_assessment_preparation
            self.metrics[Metrics.EDGES_PREDICTION] = {
                'negative_edges_per_node': negative_edges_per_node,
                'print_erroneous_edges': print_erroneous_edges,
            }

        self.edges_to_add = edges_to_add
        self.edges_to_remove = edges_to_remove
        return self

    def with_closest_neighbours(self, test_no: int, neighbors_no: int) -> 'AssessmentConfig':
        self.metrics[Metrics.PRINT_NEIGHBOURS] = {
            'test_no': test_no,
            'neighbors_no': neighbors_no
        }
        return self

    def with_tsne_visualization(self, image_size: Tuple[int, int], dpi: int, dst_path: str) -> 'AssessmentConfig':
        self.metrics[Metrics.TSNE_VISUALIZATION] = {
            'image_size': image_size,  # (image_size[0]/dpi, image_size[1]/dpi),
            'dpi': dpi,
            'dst_path': dst_path
        }
        return self

    def with_ancestor_prediction(self, samples_per_node: int,  max_distance: int, k: int,
                                 operators: List[str] = None) -> 'AssessmentConfig':
        self.metrics[Metrics.ANCESTOR_BY_DISTANCE] = {
            'samples_per_node': samples_per_node,
            'max_distance': max_distance,
            'k': k,
            'operators': operators
        }
        return self

    def with_softmax_ancestor_prediction(self, epochs: int, learning_rate: float, batch_size: int):
        self.metrics[Metrics.ANCESTOR_WITH_SOFTMAX] = {
            'epochs': epochs,
            'learning_rate': learning_rate,
            'batch_size': batch_size
        }
        return self

    def clear(self) -> 'AssessmentConfig':
        self.metrics = {}
        return self

    def prepare_graph(self, graph: Graph):
        if Metrics.EDGES_PREDICTION not in self.metrics:
            self.logger.info('Graph preparation not needed')
            return

        self.logger.info('Preparing graph')
        removed_edges = set()
        if self.edges_to_remove > 0:
            multiple_parents_nodes = list(filter(lambda n: len(n.parents) > 1, graph.nodes.values()))
            while len(removed_edges) < self.edges_to_remove:
                child = np.random.choice(multiple_parents_nodes)
                parent = np.random.choice(child.parents)

                child.parents.remove(parent)
                parent.children.remove(child)
                removed_edges.add((parent.idx, child.idx))
                self.logger.debug('Removing edge from: \"%s\" to \"%s\"', parent.name, child.name)

                multiple_parents_nodes.remove(child)
                if len(multiple_parents_nodes) == 0:
                    self.logger.warning('No more edges to remove. Stopping.')
                    break
            self.logger.info('Removed %d edges from graph', len(removed_edges))
            self.metrics[Metrics.EDGES_PREDICTION]['removed_edges'] = removed_edges
        else:
            self.metrics[Metrics.EDGES_PREDICTION]['removed_edges'] = None

        if self.edges_to_add > 0:
            added_edges = set()
            nodes = list(graph.nodes.values())
            while len(added_edges) < self.edges_to_add:
                parent = np.random.choice(nodes)
                child = np.random.choice(nodes)
                if parent == child or \
                        child in parent.children or \
                        (parent.idx, child.idx) in removed_edges:
                    continue
                parent.add_child(child)
                child.add_parent(parent)
                added_edges.add((parent.idx, child.idx))

                self.logger.debug('Adding edge from: \"%s\" to \"%s\"', parent.name, child.name)

            self.logger.info('Added %d edges to graph', len(added_edges))
            self.metrics[Metrics.EDGES_PREDICTION]['added_edges'] = added_edges
        else:
            self.metrics[Metrics.EDGES_PREDICTION]['added_edges'] = None


class _WalksMemento:
    """
    Class that handles creation and maintenance of random walks for given graph.
    Walks are recreated only when passed configuration changes from previous one.
    """

    def __init__(self, graph: Graph) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.graph = graph
        self.random_walks = None
        self.walks_per_node = None
        self.walk_length = None
        self.restart_probability = None

    def get_random_walks(self, **kwargs):
        """
        Returns random walks based on given configuration
        :param kwargs: configuration passed to :func:`~cat2vec.embedding.embedding_model.EmbeddingModel.fit_args`
        :return:
        """
        use_random_walks, (walks_per_node, walk_length, restart_probability) = self._parse_kwargs(**kwargs)

        if not use_random_walks:
            return None
        else:
            if self._is_update_needed(walks_per_node, walk_length, restart_probability) or self.random_walks is None:
                self.logger.info('Creating new random walks')
                self.random_walks = random_walk.random_walks(self.graph,
                                                             walks_per_node=walks_per_node,
                                                             walk_length=walk_length,
                                                             restart_probability=restart_probability)
            return self.random_walks

    def _parse_kwargs(self, **kwargs):
        walks_per_node = kwargs['walks_per_node'] if 'walks_per_node' in kwargs else None
        walk_length = kwargs['walk_length'] if 'walk_length' in kwargs else None
        restart_probability = kwargs['restart_probability'] if 'restart_probability' in kwargs else None

        use_random_walks = False
        if walks_per_node is not None or walk_length is not None or restart_probability is not None:
            self.logger.debug('Random walks creation configuration found')
            assert walks_per_node is not None
            assert walk_length is not None
            assert restart_probability is not None
            use_random_walks = True

        return use_random_walks, (walks_per_node, walk_length, restart_probability)

    def _is_update_needed(self, walks_per_node, walk_length, restart_probability):
        update_needed = False

        if self.walks_per_node != walks_per_node:
            update_needed = True
            self.walks_per_node = walks_per_node
        if self.walk_length != walk_length:
            update_needed = True
            self.walk_length = walk_length
        if self.restart_probability != restart_probability:
            update_needed = True
            self.restart_probability = restart_probability
        return update_needed
