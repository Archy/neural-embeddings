import logging
import re
from collections import defaultdict
from typing import Dict, Tuple, List

from cat2vec.graph.graph import Graph


logger = logging.getLogger(__name__)


def load_graph(category_filepath: str, page_filepath, categorylinks_filepath: str) -> Graph:
    logger.info('Loading graph from raw dump: %s, %s, %s', category_filepath, page_filepath, categorylinks_filepath)

    graph = _load_categories(category_filepath)
    page_id2name = _load_page_ids(page_filepath, graph)
    links = _load_subcategory_to_category_links(categorylinks_filepath)
    _link_nodes(graph, page_id2name, links)

    return graph


def load_categories_per_article(categorylinks_filepath) -> Dict[int, int]:
    pages_links = list(map((lambda x: x[0]), _load_page_to_category_links(categorylinks_filepath)))
    pages_categories = defaultdict(lambda: 0)

    for page in pages_links:
        pages_categories[page] += 1

    return pages_categories


def _link_nodes(graph: Graph, page_id2name: Dict[int, str], links: List[Tuple[int, str]]):
    """
    Creates links in the graph
    """
    name2idx = {v.name: k for k, v in graph.nodes.items()}
    for src_page_id, dst_category_name in links:
        dst_id = name2idx[dst_category_name]
        if src_page_id in graph.nodes:
            src_id = graph.nodes[src_page_id].idx
            graph.add_edges(src_id, [dst_id])
        elif src_page_id in page_id2name:
            src_id = name2idx[page_id2name[src_page_id]]
            graph.add_edges(src_id, [dst_id])
        else:
            pass  # can happen if src wasn't in category table


_INSERT_CATEGORY_STATEMENT = 'INSERT INTO `category` VALUES '
_CATEGORY_PATTERN = r'\((\d+),\'([^\']+)\',(\d+),\d+,\d+\)'
_CATEGORY_REGEX = re.compile(_CATEGORY_PATTERN)


def _load_categories(category_filepath: str) -> Graph:
    """
    Loads data from 'enwiki-<date>-categorylinks.sql'. The data has following format:
        id                                  int
        category name                       str
        number of pages in category         int
        number of sub-categories            int
        number of files (eg. images)        int
        <reserved field for future use      int
    """
    logger.info("Parsing categories.sql")
    graph = Graph()

    with open(category_filepath, mode='rb') as categories_file:
        for line_bytes in categories_file:
            line = line_bytes.decode("latin-1")
            if line.startswith(_INSERT_CATEGORY_STATEMENT):
                line = line[len(_INSERT_CATEGORY_STATEMENT):]
                for category_match in _CATEGORY_REGEX.finditer(line):
                    category_id = int(category_match.group(1))
                    category_name = category_match.group(2)
                    number_of_articles = int(category_match.group(3))
                    graph.add_node(category_id, category_name)
                    graph.nodes[category_id].number_of_articles = number_of_articles

    logger.info("Categories.sql parsed")
    return graph


_INSERT_PAGE_STATEMENT = 'INSERT INTO `page` VALUES '
_PAGE_PATTERN = r'\((\d+),\d+,\'([^\']+)\',\'[^\']*\',\d+,\d+,\d+\.\d+,\'[^\']+\',\'[^\']+\',\d+,\d+,\'[^\']+\',[^)]+\)'
_PAGE_REGEX = re.compile(_PAGE_PATTERN)


def _load_page_ids(page_filepath: str, graph: Graph) -> Dict[int, str]:
    """
    Loads data from 'enwiki-<date>-page.sql'. The data has following format:
        page_id                 int
        page_namespace          int
        page_title              str
        page_restrictions       str
        page_is_redirect        int
        page_is_new             int
        page_random             double
        page_touched            str
        page_links_updated      str
        page_latest             int
        page_len                int
        page_content_model      str
        page_content_model      str
    """
    logger.info("Parsing page.sql")

    categories_names = set(map(lambda node: node.name, graph.nodes.values()))
    page_id2name = dict()

    with open(page_filepath, mode='rb') as categories_file:
        for line_bytes in categories_file:
            line = line_bytes.decode("latin-1")
            if line.startswith(_INSERT_PAGE_STATEMENT):
                line = line[len(_INSERT_PAGE_STATEMENT):]
                for page_match in _PAGE_REGEX.finditer(line):
                    page_id = int(page_match.group(1))
                    page_name = page_match.group(2)
                    if page_name in categories_names:
                        page_id2name[page_id] = page_name
    logger.info("page.sql parsed")
    return page_id2name


_INSERT_LINK_STATEMENT = 'INSERT INTO `categorylinks` VALUES '
_LINK_PATTERN = r'\((\d+),(\'[^\']+\'),(\'[^\']*\',){4}(\'[^\']*\')\)'
_LINK_REGEX = re.compile(_LINK_PATTERN)


def _load_subcategory_to_category_links(categorylinks_filepath: str) -> List[Tuple[int, str]]:
    logger.info("Loading page to category links")
    return _parse_categorylinks(categorylinks_filepath, '\'subcat\'')


def _load_page_to_category_links(categorylinks_filepath: str) -> List[Tuple[int, str]]:
    logger.info("Loading page to category links")
    return _parse_categorylinks(categorylinks_filepath, '\'page\'')


def _parse_categorylinks(categorylinks_filepath: str, src_type: str) -> List[Tuple[int, str]]:
    """
    Loads data from 'enwiki-<date>-categorylink.sql'. The data has following format:
        page_id of source page / direct id of category          int
        name of destination category                            str
        cl_sortkey                                              str
        cl_sortkey_prefix                                       str
        cl_timestamp                                            str
        cl_collation                                            str
        type of source page (one of: page, file, subcat):       str
    """
    logger.info("Parsing categorylinks.sql")
    links = []

    with open(categorylinks_filepath, mode='rb') as categories_file:
        for line_bytes in categories_file:
            line = line_bytes.decode("latin-1")
            if line.startswith(_INSERT_LINK_STATEMENT):
                line = line[len(_INSERT_LINK_STATEMENT):]
                for page_match in _LINK_REGEX.finditer(line):

                    link_type = page_match.group(4)
                    if link_type == src_type:
                        src_page_id = int(page_match.group(1))
                        dts_category_name = page_match.group(2).strip('\'')
                        links.append((src_page_id, dts_category_name))

    logger.info("categorylinks.sql parsed. Total links loaded: %d", len(links))
    return links
