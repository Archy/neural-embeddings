import logging
from typing import Set

from cat2vec.graph.graph import Graph, Node

logger = logging.getLogger(__name__)


def load_graph(categoriels_filepath: str, cat_dict_filepath: str, art_filepath: str = None, root: str = None) -> Graph:
    """
    Loads graph from MATRIX'u files. Cat_dict file contains mapping from category name to id.
    Categories contains file with relation between categories in form:
    <parent id> [children id's] [0's - not sure what they represents]

    :param categoriels_filepath: path to file named like: history-of-poland-10-cats_links-en-20111201
    :param cat_dict_filepath: path to file named like: history-of-poland-10-po_linkach-cats_dict-en-20111201
    :param art_filepath: optional path to file named like: pl_ww2-po_linkach-cats_dict-en-20111201
    :param root: graph's root node name. Must be passed if there might be disconnected subgraphs in the graph;
    :return: loaded graph
    """
    logger.info('Loading graph from matrix\' files: %s, %s', categoriels_filepath, cat_dict_filepath)
    graph = Graph()
    root_idx = None

    with open(cat_dict_filepath, encoding='UTF-8') as cat_dict:
        for line in cat_dict:
            temp = line.split()
            assert len(temp) == 2
            graph.add_node(int(temp[1]), temp[0])
            if root is not None and root == temp[0]:
                root_idx = int(temp[1])

    if root_idx is not None:
        graph.root_idx = root_idx

    with open(categoriels_filepath, encoding='UTF-8') as edges:
        for line in edges:
            if line.strip():
                temp = line.split()
                src, temp = temp[0], temp[1:]
                graph.add_edges(int(src), list(map(int, temp)))

    if root is not None:
        _clear_graph(graph, root_idx)

    if art_filepath is not None:
        _load_articles(graph, art_filepath)

    logger.info('Loaded graph with %d nodes and %d edges', len(graph.nodes.keys()), graph.count_edges())
    return graph


def _load_articles(graph: Graph, art_filepath: str) -> None:
    """
    Assigns articles from to categories (graph nodes)

    :param graph: the graph
    :param art_filepath: path to file named like pl_ww2-po_linkach-categories-en-20111201
    """
    logger.info('Loading articles from %s', art_filepath)

    links_count = 0
    total_articles = 0
    with open(art_filepath, encoding='UTF-8') as f:
        for line in f:
            tmp = line.split()
            art = int(tmp[0])
            cats = list(map(lambda x: int(x), tmp[1:]))
            if len(cats) < 2:  # article with only 1 category assigned is useless for us
                continue

            article_added = 0
            for c in cats:
                if c not in graph.nodes:
                    logger.debug('\tCategory not found: ', c)  # can be some trash from MATRIXu
                    continue
                graph.nodes[c].articles.add(art)
                links_count += 1
                article_added = 1
            total_articles += article_added

    graph.articles_loaded = True
    graph.total_articles = total_articles
    logger.info('Loaded %d articles and %d articles links', total_articles, links_count)


def _clear_graph(graph: Graph, root_idx: int):
    """
    ATM we assume that graph must be coherent. Removes all disconnected nodes
    """
    logger.info('Cleaning graph')

    disconnected = set(graph.nodes.keys())
    root = graph.nodes[root_idx]
    _rec_clear(root, disconnected)

    logger.debug('Removing %d disconnected nodes from graph', len(disconnected))
    for d in disconnected:
        logger.debug('Removing node %d', d)
        node = graph.nodes.pop(d)
        for child in node.children:
            child.parents.remove(node)


def _rec_clear(node: Node, disconnected: Set[int]):
    if node.idx in disconnected:
        disconnected.remove(node.idx)
        for c in node.children:
            _rec_clear(c, disconnected)
