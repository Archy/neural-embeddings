from cat2vec.graph.graph import Graph
import networkx as nx


def to_graphml(graph: Graph, dst_path: str) -> None:
    """
    Saves give graph to given path in graphml format
    :param graph: the graph
    :param dst_path: destination file path
    """
    nx_graph = nx.Graph()

    for node in graph.nodes.values():
        nx_graph.add_node(node.idx, name=node.name)

    for node in graph.nodes.values():
        for child in node.children:
            nx_graph.add_edge(node.idx, child.idx)

    nx.write_graphml(nx_graph, dst_path)
