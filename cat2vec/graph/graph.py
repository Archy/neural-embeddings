import logging
from functools import reduce
from typing import Dict, List, Set


class Graph:
    def __init__(self, root_idx: int = None) -> None:
        super().__init__()
        self.logger = logging.getLogger(__name__)

        self.nodes: Dict[int, Node] = {}
        self.articles_loaded = False
        self.total_articles = 0
        self.root_idx = root_idx

    def add_node(self, idx: int, name: str) -> None:
        assert idx not in self.nodes
        assert idx != 0  # this would break adding edges
        self.nodes[idx] = Node(idx, name)

    def add_edges(self, src: int, destinations: List[int]):
        if src not in self.nodes:
            self.logger.debug('src vertex %d doesn\'t exists', src)
            return

        for dst in destinations:
            if dst == 0:
                continue  # some garbage from MATRIX'u

            if dst in self.nodes:
                self.logger.debug('Adding edge from %d to %d', src, dst)
                self.nodes[src].add_child(self.nodes[dst])
                self.nodes[dst].add_parent(self.nodes[src])
            else:
                self.logger.debug('dst vertex %d doesn\'t exists', dst)

    def count_edges(self) -> int:
        return reduce(lambda x, y: x + y, map(lambda n: len(n.children), self.nodes.values()))

    def __eq__(self, other: object) -> bool:
        """Overrides the default implementation"""
        if isinstance(other, Graph):
            return self.nodes == other.nodes
        return False

    def __ne__(self, o: object) -> bool:
        """Overrides the default implementation"""
        return not self.__eq__(o)


class Node:
    def __init__(self, idx: int, name: str) -> None:
        super().__init__()
        self.idx = idx
        self.name = name
        self.children: List[Node] = []
        self.parents: List[Node] = []
        self.articles: Set[int] = set()

    # 'Node' -> see https://stackoverflow.com/questions/49392845/python-parameter-annotations-unresolved-reference
    def add_child(self, child: 'Node'):
        self.children.append(child)

    def add_parent(self, parent: 'Node'):
        self.parents.append(parent)

    def __eq__(self, other: object) -> bool:
        """Overrides the default implementation"""
        if isinstance(other, Node):
            return self.idx == other.idx \
                   and self.name == other.name \
                   and self.children == other.children
        return False

    def __hash__(self) -> int:
        return hash(self.idx)

    def __ne__(self, o: object) -> bool:
        """Overrides the default implementation"""
        return not self.__eq__(o)

    def __str__(self) -> str:
        children = ''
        if len(self.children) > 0:
            children = 'children: [' + reduce(lambda x, y: x + ', ' + y, map(lambda c: str(c.idx), self.children)) + ']'
        return '<Node ' + str(self.idx) + ' ' + children + '>'
