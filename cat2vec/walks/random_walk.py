import random
from typing import List

from cat2vec.graph.graph import Graph, Node


def random_walk(graph: Graph, walk_length: int, start: Node = None, restart_probability: float = 0) -> List[int]:
    """
    Creates a single random walk
    :return: list of nodes ids representing the walk
    """
    walk = []

    if start is None:
        start = random.sample(graph.nodes.items(), 1)[0][1]
    walk.append(start.idx)

    last = start
    for i in range(1, walk_length):
        if len(last.children) == 0:
            break
        if random.random() > restart_probability:  # move to next child
            last = random.sample(last.children, 1)[0]
        else:  # restart walk
            last = start
        walk.append(last.idx)

    return walk


def random_walks(graph: Graph, walks_per_node: int, walk_length: int,
                 restart_probability: float = 0) -> List[List[int]]:
    """
    Creates series of random walks starting from each vertex. Walks won't be shorter than 2.
    :return: list of random walks
    """

    walks = []
    nodes = list(graph.nodes.keys())

    for i in range(walks_per_node):
        random.shuffle(nodes)
        for idx in nodes:
            walk = random_walk(graph, walk_length, graph.nodes[idx], restart_probability)
            if len(walk) >= 2:
                walks.append(walk)
    return walks
