import json
from enum import Enum
import numpy as np


class EnumEncoderJsonAware(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Enum):
            return o.name
        elif isinstance(o, np.integer):
            return int(o)
        elif isinstance(o, np.floating):
            return float(o)
        elif isinstance(o, np.ndarray):
            return o.tolist()
        return json.JSONEncoder.default(self, o)
