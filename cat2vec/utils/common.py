import logging

import numpy as np
import os
from typing import Dict, Optional, List
from nltk.tokenize import WordPunctTokenizer
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag
from nltk.corpus import wordnet
import string

from cat2vec.graph.graph import Graph

GLOVE_6B_BASE_PATH = r'C:\Users\Jan\Desktop\neural-embeddings\glove.6B'
GLOVE_BASE_NAME = 'glove.6B.%dd.txt'

logger = logging.getLogger(__name__)
word_punct_tokenizer = WordPunctTokenizer()
wordnet_lemmatizer = WordNetLemmatizer()


def create_node_idx_mapping(graph: Graph) -> Dict[int, int]:
    """
    Creates a dictionary that maps nodes id's (any integers sequence)
    to ids from a consecutive integer sequence starting form 0
    """
    idx = 0
    word2idx = {}
    for key in graph.nodes:
        word2idx[key] = idx
        idx += 1
    return word2idx


def create_initial_weight(graph: Graph, node2idx: Dict[int, int],
                          embedding_size: int, use_glove_init: bool = False) -> np.ndarray:
    """
    Creates initial weights of shape: no_of_nodes x embedding_size.
    If use_glove_init is true uses Glove6B to initialize the weights.
    Otherwise weighs initialized with random numbers from a uniform distribution.

    This function is intended to be called once per whole program
    """
    V = len(graph.nodes)
    D = embedding_size

    if not use_glove_init:
        logger.info('Initializing weights with random values')
        return np.random.rand(V, D).astype(np.float32)

    logger.info('Using GloVe to initialize weights')

    # use glove init:
    glove_file = os.path.join(GLOVE_6B_BASE_PATH, GLOVE_BASE_NAME % embedding_size)
    word2coeffs = load_glove(glove_file)

    embeddings = np.random.rand(V, D).astype(np.float32)

    for idx, node in graph.nodes.items():
        words = _tokenize_category_name(node.name)
        e = np.zeros(D)
        any_word_match = False

        for word in words:
            if word not in word2coeffs:
                logger.debug('No match for: %s', word)
                continue
            any_word_match = True
            word_embedding = word2coeffs[word]
            e += word_embedding

        if not any_word_match:
            logger.warning('No embedding for: \'%s\'. Creating random embedding', node.name)
            e = np.random.randn(D)

        e_idx = node2idx[idx]
        embeddings[e_idx, :] = e

    return embeddings


def load_glove(glove_file: str):
    """Loads Glove6B into a dictionary"""
    if not os.path.isfile(glove_file):
        raise Exception('Glove file \'%s\' doesnt exists' % glove_file)

    word2coeffs = {}
    with open(glove_file, encoding='UTF-8') as glove:
        for line in glove:
            tmp = line.split()
            word = tmp[0]
            coeffs = np.asarray(tmp[1:], dtype='float32')
            word2coeffs[word] = coeffs

    return word2coeffs


def _tokenize_category_name(category: str) -> List[str]:
    """
    Replace _ with spaces, tokenize, lowercase and lematize the category name

    :param category: category name
    :return: list of words
    """

    tokens = word_punct_tokenizer.tokenize(category.replace('\\\'', '\'').replace('_', ' ').lower())
    pos_tagged = pos_tag(tokens)

    lemmas = []
    for w, pos in pos_tagged:
        if w in string.punctuation:
            continue
        wordnet_pos_tag = _convert_pos_tag(pos)
        if wordnet_pos_tag:
            lemmas.append(wordnet_lemmatizer.lemmatize(w, pos=wordnet_pos_tag))
        else:
            lemmas.append(wordnet_lemmatizer.lemmatize(w))

    return lemmas


def _convert_pos_tag(treebank_pos_tag: str) -> Optional[str]:
    """
    Default NLTK pos tagger uses Treebank part-of-speech tags set
    (see https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html)
    WordNetLemmatizer expects WordNet part of speech names so convertion is required.
    """
    treebank_pos_tag = treebank_pos_tag.upper()
    if treebank_pos_tag.startswith('J'):
        return wordnet.ADJ
    if treebank_pos_tag.startswith('V'):
        return wordnet.VERB
    if treebank_pos_tag.startswith('N'):
        return wordnet.NOUN
    if treebank_pos_tag.startswith('R'):
        return wordnet.ADV
    return None
